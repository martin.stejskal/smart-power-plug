# About
 * Target is to have "smart" power plug driven by ESP32, which already include
   WiFi and Bluetooth.

 ![index](doc/2019.11.05_19.29.53_index.jpg)

# How it works
 * The ESP32 scan for BT devices. When specific device appears, enable power
   plug.
 * When device disappears, power off power plug. At least in principle. Of
   course whole device is bit more sophisticated.
 * User can press button to manually enable/disable power plug

# What do I need to do?
## Build hardware
 * Check [exported](hw/export) files. There is schematic, gerber data and BOM.
 * Version *00* is obsolete and have some problems. To avoid hair loss, build
   version **01**.
 * Alternatively you can use any *ESP32* development kit and connect it with
   "triac" module from ebay (actually on board is triac) just to test it.
 * Basically ESP's GPIO *IO2* control optocoupler, which control AC part.
 * Other components are there to make it more fancy - automatic reset and boot
   when programming, LED to make it shiny and button to give user power over
   whole system.

## Get sources or binaries
 * Build or
   [download binary](https://gitlab.com/martin.stejskal/smart-power-plug/pipelines)
   . When downloading binaries, on right side there are *"artifacts"* which are
   automatically build. You can download it directly.

   ![Download artifacts](doc/download_artifacts.png)
   
 * If you decide to use binaries, please refer to *README.md* file in
   release and go back when you have flashed ESP32.
 * If you want to build from sources and you have
   [ESP IDF](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/index.html)
   and toolchain installed, just run following (otherwise visit link):

```bash
$ cd fw/
$ source source_me.sh  # Install tools to virtual env, setup PATH
$ idf.py build flash
```

## Set trigger Bluetooth MAC
 * Now you need to change default values - at least set trigger Bluetooth MAC.
 * You will need to connect to secondary (user) UART on ESP32. In schematic it
   is pinheader named `User_Interface`.
 * By default on GPIO `17` is **TX** and on GPIO `16` is **RX** for user UART.
   UART runs at `115200 bps`.
 * Once you connected, press `Enter` key and help should appear. Please take
   at least 30 seconds to read it.
 * Set trigger Bluetooth MAC address. Example: `bt mac 11:22:33:44:55`
 * System reboots, load new values and you're ready. Settings is stored in
   flash, so you need to set it once ;)

# Troubleshooting
## Annoying high pitch sound from AC/DC power supply
 * This is typical for low current loads on power supply output.
 * Workaround is to add bit more load to power supply. You can simply add some
   resistor directly to power supply or you can solder few resisotrs
   **between test points and GND**.

 ![Additional load](doc/2020.05.23_19.10.34_ext_load.jpg)

 * Recommened is to use 330 R (0603) resisotrs. This will allow to scale load
   by 10 mA steps.
 * Connect to UI ([Set trigger Bluetooth MAC](#set-trigger-bluetooth-mac))
   and type `ext load 1`. This will set *TP10* to high level and current will
   flow via that resisotr. If you want increase current consumption simply
   change value to 2 (*TP10, TP11*), 3 (*TP10, TP11, TP12*) or 4
   (*TP10, TP11, TP12, TP13*). Value 0 disable any extra load (default).
 * Note that this dummy load should not be high, because it is basically
   wasting energy.
 * You can reduce resistor value to increase load value, but always read ESP
   limitations before you do to avoid burning out your GPIO.

# Development notes
## Why not WiFi
 * In theory higher power consumption.
 * The 5 GHz band is not supported.
 * Even in 2.4 GHz band in sniffer mode, when no data are sent, it is not
   possible to determinate if device is enabled or disabled.
 * Due channel switching (to be universal device) it is not possible to catch
   all packets -> high chance that when WiFi will be used we loose some packets
   anyway.

## Why Bluetooth
 * Active Bluetooth devices simply reply to scan requests -> easy to detect if
   device is still online or not.
 * In theory lower power consumption.
 * In theory when Low Energy feature is used, passive scan can be used. However
   that require LE feature on both sides and some older devices does not
   support that feature :/
