/**
 * @file
 * @author Martin Stejskal
 * @brief Keep settings in non-volatile-storage memory
 */
//================================| Includes |================================
#include "nvs_settings.h"

#include <string.h>

#include "nvs_settings_config.h"

// Standard ESP libraries
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>

// ================================| Defines |================================
#define STORAGE_NAMESPACE "storage"
#define NVS_STTNGS_GET "NVS sttngs get"
#define NVS_STTNGS_SET "NVS sttngs set"
// ============================| Default values |=============================
//===========================| Preprocessor checks |==========================
//============================| Structures, enums |===========================
//============================| Global variables |============================
//================================| Functions |===============================

// =====================| Internal function prototypes |======================
static teNvsSttngsErr _nvsGet(const char *pacKey, void *pOutValue,
                              const void *pDefaultValue, size_t *piSize);
static teNvsSttngsErr _nvsSet(const char *pacKey, void *pInValue, size_t iSize);
static teNvsSttngsErr _nvsDel(const char *pacKey);
// =========================| High level functions |==========================
teNvsSttngsErr nvsSttngsGetBtMacList(
    uint8_t au8loadedMac[NVS_MAX_REGISTERED_MAC][MAC_SIZE_BYTES]) {
  uint8_t au8defaultMacList[NVS_MAX_REGISTERED_MAC][MAC_SIZE_BYTES];

  // It is expected that MAC FF:FF:FF:FF:FF:FF is invalid/empty -> by default
  // no MAC is registered
  memset(au8defaultMacList, 0xFF, sizeof(au8defaultMacList));

  size_t iReqSize = NVS_MAX_REGISTERED_MAC * MAC_SIZE_BYTES;
  return (_nvsGet("MAC", au8loadedMac, au8defaultMacList, &iReqSize));
}

teNvsSttngsErr nvsSttngsSetBtMacList(
    uint8_t au8mac[NVS_MAX_REGISTERED_MAC][MAC_SIZE_BYTES]) {
  return (_nvsSet("MAC", au8mac, NVS_MAX_REGISTERED_MAC * MAC_SIZE_BYTES));
}

teNvsSttngsErr nvsSttngsDelMacList(void) { return (_nvsDel("MAC")); }

teNvsSttngsErr nvsSttngsGetScanDuration(uint8_t *pu8loadedDuration,
                                        uint8_t u8defaultDuration) {
  size_t iReqSize = sizeof(u8defaultDuration);
  return (_nvsGet("scnDur", pu8loadedDuration, &u8defaultDuration, &iReqSize));
}

teNvsSttngsErr nvsSttngsSetScanDuration(uint8_t u8duration) {
  return (_nvsSet("scnDur", &u8duration, sizeof(u8duration)));
}

teNvsSttngsErr nvsSttngsDelScanDuration(void) { return (_nvsDel("scnDur")); }

teNvsSttngsErr nvsSttngsGetScanPeriod(uint16_t *pu16loadedPeriod,
                                      uint16_t u16defaultPeriod) {
  size_t iReqSize = sizeof(u16defaultPeriod);
  return (_nvsGet("scnPer", pu16loadedPeriod, &u16defaultPeriod, &iReqSize));
}

teNvsSttngsErr nvsSttngsSetScanPeriod(uint16_t u16period) {
  return (_nvsSet("scnPer", &u16period, sizeof(u16period)));
}

teNvsSttngsErr nvsSttngsDelScanPeriod(void) {
  teNvsSttngsErr eRetCode = _nvsDel("scnPer");
  return eRetCode;
}

teNvsSttngsErr nvsSttngsGetMinTxPwr(int8_t *pi8pwrDbm, int8_t iDefaultPwrDbm) {
  size_t iReqSize = sizeof(iDefaultPwrDbm);
  return (_nvsGet("MinTxPwr", pi8pwrDbm, &iDefaultPwrDbm, &iReqSize));
}

teNvsSttngsErr nvsSttngsSetMinTxPwr(int8_t i8pwrDbm) {
  return (_nvsSet("MinTxPwr", &i8pwrDbm, sizeof(i8pwrDbm)));
}
teNvsSttngsErr nvsSttngsDelMinTxPwr(void) { return (_nvsDel("MinTxPwr")); }

teNvsSttngsErr nvsSttngsGetMaxTxPwr(int8_t *pi8pwrDbm, int8_t iDefaultPwrDbm) {
  size_t iReqSize = sizeof(iDefaultPwrDbm);
  return (_nvsGet("MaxTxPwr", pi8pwrDbm, &iDefaultPwrDbm, &iReqSize));
}

teNvsSttngsErr nvsSttngsSetMaxTxPwr(int8_t i8pwrDbm) {
  return (_nvsSet("MaxTxPwr", &i8pwrDbm, 1));
}

teNvsSttngsErr nvsSttngsDelMaxTxPwr(void) { return (_nvsDel("MaxTxPwr")); }

teNvsSttngsErr nvsSttngsGetDropOffCnt(uint8_t *pu8dropOffCnt,
                                      uint8_t u8defaultDropOffCnt) {
  size_t iReqSize = sizeof(u8defaultDropOffCnt);

  return (
      _nvsGet("DropOffCnt", pu8dropOffCnt, &u8defaultDropOffCnt, &iReqSize));
}
teNvsSttngsErr nvsSttngsSetDropOffCnt(uint8_t u8dropOffCnt) {
  return (_nvsSet("DropOffCnt", &u8dropOffCnt, sizeof(u8dropOffCnt)));
}
teNvsSttngsErr nvsSttngsDelDropOffCnt(void) { return (_nvsDel("DropOffCnt")); }

teNvsSttngsErr nvsSttngsGetExtraLoad(uint8_t *pu8loadLevel,
                                     uint8_t u8defaultLoadLevel) {
  size_t iReqSize = sizeof(u8defaultLoadLevel);
  return (_nvsGet("ExtraLoad", pu8loadLevel, &u8defaultLoadLevel, &iReqSize));
}
teNvsSttngsErr nvsSttngsSetExtraLoad(uint8_t u8loadLevel) {
  return (_nvsSet("ExtraLoad", &u8loadLevel, sizeof(u8loadLevel)));
}
teNvsSttngsErr nvsSttngsDelExtraLoad(void) { return (_nvsDel("ExtraLoad")); }
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
/**
 * @brief Get value from NVM
 * @param pacKey Key identificator
 * @param pOutValue Result will be written there
 * @param pDefaultValue Default value for case that value was not written to
 *                      EEPROM yet
 * @param piSize Size of value in Bytes
 * @return Zero if there is no problem
 */
static teNvsSttngsErr _nvsGet(const char *pacKey, void *pOutValue,
                              const void *pDefaultValue, size_t *piSize) {
  nvs_handle_t sNvsHandler;
  teNvsSttngsErr eRetCode = NVS_STTNGS_OK;

  esp_err_t eErr = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &sNvsHandler);
  if (eErr) {
    // Use default & exit function - can not read anyway
    ESP_LOGE(NVS_STTNGS_GET, "Opening NVS failed: %d", eErr);
    return NVS_STTNGS_OPEN_ERR;
  }

  eErr = nvs_get_blob(sNvsHandler, pacKey, pOutValue, piSize);
  if (eErr) {
    // Use default
    memcpy(pOutValue, pDefaultValue, *piSize);

    if (eErr == ESP_ERR_NVS_NOT_FOUND) {
      ESP_LOGW(NVS_STTNGS_GET, "Reading failed - variable not set yet");
      eRetCode = NVS_STTNGS_VARIABLE_NOT_EXIST_YET;
    } else {
      ESP_LOGE(NVS_STTNGS_GET, "Reading from NVS failed: %d", eErr);
      eRetCode = NVS_STTNGS_READ_ERR;
    }
  }
  // Close in any case
  nvs_close(sNvsHandler);

#if NVS_STTNGS_VAL_NOT_EXIST_IS_OK
  if (eRetCode == NVS_STTNGS_VARIABLE_NOT_EXIST_YET) {
    return NVS_STTNGS_OK;
  }
#endif

  return eRetCode;
}

/**
 * @brief Write key and value to NVS
 * @param pacKey Key identificator
 * @param pInValue Key value
 * @param iSize Value size in Bytes
 * @return Zero if there is no problem
 */
static teNvsSttngsErr _nvsSet(const char *pacKey, void *pInValue,
                              size_t iSize) {
  nvs_handle_t sNvsHandler;
  teNvsSttngsErr eRetCode = NVS_STTNGS_OK;

  esp_err_t eErr = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &sNvsHandler);
  if (eErr) {
    ESP_LOGE(NVS_STTNGS_SET, "Opening NVS failed: %d", eErr);
    return NVS_STTNGS_OPEN_ERR;
  }

  eErr = nvs_set_blob(sNvsHandler, pacKey, pInValue, iSize);
  if (eErr) {
    ESP_LOGE(NVS_STTNGS_SET, "Writing to NVS failed: %d", eErr);
    eRetCode = NVS_STTNGS_WRITE_ERR;
  } else {
    // Commit write
    eErr = nvs_commit(sNvsHandler);
    if (eErr) {
      eRetCode = NVS_STTNGS_WRITE_ERR;
    }
  }
  // Close anyway
  nvs_close(sNvsHandler);

  return eRetCode;
}

static teNvsSttngsErr _nvsDel(const char *pacKey) {
  nvs_handle_t sNvsHandler;
  teNvsSttngsErr eRetCode = NVS_STTNGS_OK;

  esp_err_t eErr = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &sNvsHandler);
  if (eErr) {
    ESP_LOGE(NVS_STTNGS_SET, "Opening NVS failed: %d", eErr);
    return NVS_STTNGS_OPEN_ERR;
  }

  eErr = nvs_erase_key(sNvsHandler, pacKey);

  if (eErr == ESP_ERR_NVS_NOT_FOUND) {
    // Key does not exists -> kind of OK
    eRetCode = NVS_STTNGS_OK;
  } else if (eErr == ESP_OK) {
    // Delete successful. Commit changes
    eErr = nvs_commit(sNvsHandler);
    if (eErr) {
      eRetCode = NVS_STTNGS_WRITE_ERR;
    }
  } else if (eErr != ESP_OK) {
    // Some another error
    eRetCode = NVS_STTNGS_CAN_NOT_ERASE_KEY;
  }

  // Close anyway
  nvs_close(sNvsHandler);

  return eRetCode;
}
