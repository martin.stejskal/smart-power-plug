/**
 * @file
 * @author Martin Stejskal
 * @brief Keep settings in non-volatile-storage memory
 */
#ifndef __NVS_SETTINGS_H__
#define __NVS_SETTINGS_H__
//================================| Includes |================================
#include <inttypes.h>

#include "mac.h"
// ================================| Defines |================================
/**
 * @brief Tells how many MAC can device keep in NVS memory
 */
#define NVS_MAX_REGISTERED_MAC (50)
// ============================| Default values |=============================
//===========================| Preprocessor checks |==========================
//============================| Structures, enums |===========================
typedef enum {
  NVS_STTNGS_OK = 0,                  //!< NVS_STTNGS_OK
  NVS_STTNGS_OPEN_ERR,                //!< NVS_STTNGS_OPEN_ERR
  NVS_STTNGS_READ_ERR,                //!< NVS_STTNGS_READ_ERR
  NVS_STTNGS_WRITE_ERR,               //!< NVS_STTNGS_WRITE_ERR
  NVS_STTNGS_VARIABLE_NOT_EXIST_YET,  //!< NVS_STTNGS_VARIABLE_NOT_EXIST_YET
  NVS_STTNGS_CAN_NOT_ERASE_KEY,       //!< NVS_STTNGS_CAN_NOT_ERASE_KEY
} teNvsSttngsErr;
//============================| Global variables |============================
//================================| Functions |===============================
// =========================| High level functions |==========================
teNvsSttngsErr nvsSttngsGetBtMacList(
    uint8_t au8loadedMac[NVS_MAX_REGISTERED_MAC][MAC_SIZE_BYTES]);
teNvsSttngsErr nvsSttngsSetBtMacList(
    uint8_t au8writeMac[NVS_MAX_REGISTERED_MAC][MAC_SIZE_BYTES]);
teNvsSttngsErr nvsSttngsDelMacList(void);

teNvsSttngsErr nvsSttngsGetScanDuration(uint8_t *pu8loadedDuration,
                                        uint8_t u8defaultDuration);
teNvsSttngsErr nvsSttngsSetScanDuration(uint8_t u8duration);
teNvsSttngsErr nvsSttngsDelScanDuration(void);

teNvsSttngsErr nvsSttngsGetScanPeriod(uint16_t *pu16loadedPeriod,
                                      uint16_t u16defaultPeriod);
teNvsSttngsErr nvsSttngsSetScanPeriod(uint16_t u16period);
teNvsSttngsErr nvsSttngsDelScanPeriod(void);

teNvsSttngsErr nvsSttngsGetMinTxPwr(int8_t *pi8pwrDbm, int8_t iDefaultPwrDbm);
teNvsSttngsErr nvsSttngsSetMinTxPwr(int8_t i8pwrDbm);
teNvsSttngsErr nvsSttngsDelMinTxPwr(void);

teNvsSttngsErr nvsSttngsGetMaxTxPwr(int8_t *pi8pwrDbm, int8_t iDefaultPwrDbm);
teNvsSttngsErr nvsSttngsSetMaxTxPwr(int8_t i8pwrDbm);
teNvsSttngsErr nvsSttngsDelMaxTxPwr(void);

teNvsSttngsErr nvsSttngsGetDropOffCnt(uint8_t *pu8dropOffCnt,
                                      uint8_t u8defaultDropOffCnt);
teNvsSttngsErr nvsSttngsSetDropOffCnt(uint8_t u8dropOffCnt);
teNvsSttngsErr nvsSttngsDelDropOffCnt(void);

teNvsSttngsErr nvsSttngsGetExtraLoad(uint8_t *pu8loadLevel,
                                     uint8_t u8defaultLoadLevel);
teNvsSttngsErr nvsSttngsSetExtraLoad(uint8_t u8loadLevel);
teNvsSttngsErr nvsSttngsDelExtraLoad(void);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
#endif  // __NVS_SETTINGS_H__
