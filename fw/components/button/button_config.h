/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for button module
 */
#ifndef __BUTTON_CONFIG_H__
#define __BUTTON_CONFIG_H__
// ===============================| Includes |================================
// When compiler support it, check if user file exists and if yes, include it
#if defined __has_include
#  if __has_include ("button_config_user.h")
#    include "button_config_user.h"
#  endif
#endif
// ============================| Default values |=============================
#ifndef BTN_MANUAL_SWITCH
/**
 * @brief Map button which allow user to override default settings
 */
#define BTN_MANUAL_SWITCH       (GPIO_NUM_5)
#endif // BTN_MANUAL_SWITCH

#ifndef BTN_SCAN_PERIOD_MS

/**
 * @brief Approximate sample rate for scanning buttons
 *
 * @note During this period suppose to fade off all button clipping. So set
 *       this value wisely.
 */
#define BTN_SCAN_PERIOD_MS      (100)
#endif // BTN_SCAN_PERIOD_MS

#ifndef BTN_LED_BT_ON
/**
 * @brief Set GPIO for Bluetooth mode enabled indication
 */
#define BTN_LED_BT_ON           (GPIO_NUM_18)
#endif // BTN_LED_BT_ON

#ifndef BTN_LED_MANUAL_ON
/**
 * @brief Set GPIO for manual mode enabled indication
 */
#define BTN_LED_MANUAL_ON       (GPIO_NUM_19)
#endif // BTN_LED_MANUAL_ON

#ifndef BTN_LED_BTN_PRESSED
#define BTN_LED_BTN_PRESSED     (GPIO_NUM_21)
#endif // BTN_LED_BTN_PRESSED

#ifndef BTN_LED_ON_PERIOD_MS
/**
 * @brief Define how long LED suppose to be turned on
 *
 * To make power plug less annoying, all LED are off after some time. And there
 * you can define that time in milliseconds.
 */
#define BTN_LED_ON_PERIOD_MS    (15000)
#endif // BTN_LED_ON_PERIOD_MS
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
#endif // __BUTTON_CONFIG_H__
