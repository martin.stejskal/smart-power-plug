/**
 * @file
 * @author Martin Stejskal
 * @brief Module that handle user interaction with buttons
 */
#ifndef __BUTTON_H__
#define __BUTTON_H__
// ===============================| Includes |================================
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum { BTN_INIT, BTN_WAIT_FOR_INPUT } buttonStates_t;

typedef enum {
  BTN_MAN_SW_NONE,
  BTN_MAN_SW_PWR_ON,
  BTN_MAN_SW_PWR_OFF,
} buttonManualSwStatus_t;
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
/**
 * @brief Button task for RTOS
 *
 * When RTOS system is used, this task suppose to run
 *
 * @param pvParameters Pointer to parameters
 */
void buttonTaskRTOS(void *pvParameters);

/**
 * @brief Button cooperative task
 *
 * When RTOS is not available, this function suppose to be called periodically
 * or at least "often" enough to handle user interaction
 */
void buttonTask(void);
// ========================| Middle level functions |=========================
/**
 * @brief Initialize button
 * @param pStatePrevious Pointer to previous state used by state machine
 * @param pStateActual Pointer to actual state used by state machine
 */
void buttonInit(buttonStates_t *pStatePrevious, buttonStates_t *pStateActual);

/**
 * @brief Scan for pressed button
 * @param pStatePrevious Pointer to previous state used by state machine
 * @param pStateActual Pointer to actual state used by state machine
 */
void buttonWaitForInput(buttonStates_t *pStatePrevious,
                        buttonStates_t *pStateActual);
// ==========================| Low level functions |==========================
#endif  // __BUTTON_H__
