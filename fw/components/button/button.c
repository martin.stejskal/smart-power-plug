/**
 * @file
 * @author Martin Stejskal
 * @brief Module that handle user interaction with buttons
 */
// ===============================| Includes |================================
#include "button.h"

#include "button_config.h"
#include "pwr_switch.h"

// Standard ESP32 libraries
#include <driver/gpio.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// ================================| Defines |================================
#define BTN_GET_CLK_MS() (esp_log_timestamp())
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *BTN_TAG = "BTN";
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief State machine for "manual switch" logic
 */
static void _manualSwTogglePower(void);

/**
 * @brief Immediately disconnect power from power plug
 *
 * Can be called in some special cases
 */
static void _emergencyPowerOff(void);

/**
 * @brief Surprisingly, turn off all LED
 */
static void _turnOffAllLed(void);
// =========================| High level functions |==========================
void buttonTaskRTOS(void *pvParameters) {
  for (;;) {
    buttonTask();
    vTaskDelay(BTN_SCAN_PERIOD_MS / portTICK_PERIOD_MS);
  }
}

void buttonTask(void) {
  static buttonStates_t eStateActual, eStatePrevious = BTN_INIT;

  switch (eStateActual) {
    case BTN_INIT:
      buttonInit(&eStatePrevious, &eStateActual);
      break;
    case BTN_WAIT_FOR_INPUT:
      buttonWaitForInput(&eStatePrevious, &eStateActual);
      break;
    default:
      ESP_LOGE(BTN_TAG, "Internal error! Unknown state: %d", eStateActual);
  }
}
// ========================| Middle level functions |=========================
void buttonInit(buttonStates_t *pStatePrevious, buttonStates_t *pStateActual) {
  // Set button as input
  gpio_pad_select_gpio(BTN_MANUAL_SWITCH);
  gpio_set_pull_mode(BTN_MANUAL_SWITCH, GPIO_PULLUP_ONLY);
  gpio_set_direction(BTN_MANUAL_SWITCH, GPIO_MODE_INPUT);

  // Signal LED as output and turn off them
  gpio_pad_select_gpio(BTN_LED_BT_ON);
  gpio_set_direction(BTN_LED_BT_ON, GPIO_MODE_OUTPUT);

  gpio_pad_select_gpio(BTN_LED_MANUAL_ON);
  gpio_set_direction(BTN_LED_MANUAL_ON, GPIO_MODE_OUTPUT);

  gpio_pad_select_gpio(BTN_LED_BTN_PRESSED);
  gpio_set_direction(BTN_LED_BTN_PRESSED, GPIO_MODE_OUTPUT);
  // Bit exception - turned off when button released -> just set default
  gpio_set_level(BTN_LED_BTN_PRESSED, 0);

  _turnOffAllLed();

  // Change state
  *pStatePrevious = *pStateActual;
  *pStateActual = BTN_WAIT_FOR_INPUT;
}

void buttonWaitForInput(buttonStates_t *pStatePrevious,
                        buttonStates_t *pStateActual) {
  /*
   * States:
   *   * Not pressed -> no change
   *   * Pressed <= 2s -> toggle power switch
   *   * Pressed <= 5s -> off
   */

  // Keep timestamp when last scan was done
  static uint32_t u32timePreviousScanMs = 0;
  // Keep timestamp when manual switch was pressed
  static uint32_t u32timeManSwPressedMs;
  // Keep timestamp when manual switch was released
  static uint32_t u32timeManSwReleasedMs;

  // When some LED is on, this timestamp should be updated
  static uint32_t u32timeLedOnMs;

  // Previous button value. Due to pull-up, logic is inverted
  static uint8_t u8inputManSwPrev = 1;

  // When LEDs are off due to timeout, set this variable
  static uint8_t u8ledOff = 0;

  // If some LED was enabled, this variable should be changed
  uint8_t u8ledOn = 0;

  uint8_t u8inputManSwActual;
  uint32_t u32timeActualMs = BTN_GET_CLK_MS();

  // Double check timing - pointless to scan "too often"
  if ((u32timeActualMs - u32timePreviousScanMs) >= BTN_SCAN_PERIOD_MS) {
    // Store actual scan time
    u32timePreviousScanMs = u32timeActualMs;

    // If there is time to turn LED and they were not off yet
    if (!u8ledOff &&
        ((u32timeActualMs - u32timeLedOnMs) > BTN_LED_ON_PERIOD_MS)) {
      _turnOffAllLed();
      // Set status flag
      u8ledOff = 1;
    }

    // Load button state
    u8inputManSwActual = gpio_get_level(BTN_MANUAL_SWITCH);
    // If value is same -> no change -> check for some hacky scenarios
    if (u8inputManSwActual == u8inputManSwPrev) {
      if (((u32timeActualMs - u32timeManSwPressedMs) >= 5000) &&
          (!u8inputManSwActual)) {
        // Turn off - PC like shutdown (5s + button pressed)
        // Note that I/O logic have to be inverted (pull up)
        _emergencyPowerOff();
        // Some LED was enabled -> reflect change
        u8ledOn++;
      }
    } else if (u8inputManSwPrev && !u8inputManSwActual) {
      // If pressed

      u32timeManSwPressedMs = u32timeActualMs;
      // Store actual value as "previous" for next iteration
      u8inputManSwPrev = u8inputManSwActual;

      gpio_set_level(BTN_LED_BTN_PRESSED, 1);
      ESP_LOGI(BTN_TAG, "pressed");
    } else if (!u8inputManSwPrev && u8inputManSwActual) {
      // If released

      u32timeManSwReleasedMs = u32timeActualMs;

      // Store actual value as "previous" for next iteration
      u8inputManSwPrev = u8inputManSwActual;

      gpio_set_level(BTN_LED_BTN_PRESSED, 0);
      ESP_LOGI(BTN_TAG, "released");

      if ((u32timeManSwReleasedMs - u32timeManSwPressedMs) <= 3000) {
        _manualSwTogglePower();
        // Some LED was enabled -> reflect change
        u8ledOn++;
      }
    } else {
      // If we get there, there is serious problem with whole logic
      ESP_LOGE(BTN_TAG, "Internal error! %s L%d", __FILE__, __LINE__);
    }

    // If something turned on LED, take timestamp
    if (u8ledOn) {
      u8ledOff = 0;
      u32timeLedOnMs = BTN_GET_CLK_MS();
    }
  }
}
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
inline static void _manualSwTogglePower(void) {
  static buttonManualSwStatus_t eManualSwStatus = BTN_MAN_SW_NONE;

  // According to actual "switch state machine" change value
  switch (eManualSwStatus) {
    case BTN_MAN_SW_NONE:
      eManualSwStatus = BTN_MAN_SW_PWR_ON;

      gpio_set_level(BTN_LED_BT_ON, 0);
      gpio_set_level(BTN_LED_MANUAL_ON, 1);

      pwrSwSetMode(PWR_SW_MODE_MANUAL);
      pwrSwManualSet(1);
      break;
    case BTN_MAN_SW_PWR_ON:
      eManualSwStatus = BTN_MAN_SW_PWR_OFF;

      gpio_set_level(BTN_LED_BT_ON, 0);
      gpio_set_level(BTN_LED_MANUAL_ON, 1);

      pwrSwSetMode(PWR_SW_MODE_MANUAL);
      pwrSwManualSet(0);
      break;
    case BTN_MAN_SW_PWR_OFF:
      eManualSwStatus = BTN_MAN_SW_NONE;

      gpio_set_level(BTN_LED_BT_ON, 1);
      gpio_set_level(BTN_LED_MANUAL_ON, 0);

      pwrSwSetMode(PWR_SW_MODE_BT);
      break;
    default:
      // This should not happen
      ESP_LOGE(BTN_TAG, "Unexpected state for _manualSwTogglePower: %d",
               eManualSwStatus);
      // Set default value
      eManualSwStatus = BTN_MAN_SW_NONE;
  }
}

inline static void _emergencyPowerOff(void) {
  gpio_set_level(BTN_LED_BT_ON, 0);
  gpio_set_level(BTN_LED_MANUAL_ON, 1);

  pwrSwSetMode(PWR_SW_MODE_MANUAL);
  pwrSwManualSet(0);
}

static void _turnOffAllLed(void) {
  gpio_set_level(BTN_LED_BT_ON, 0);
  gpio_set_level(BTN_LED_MANUAL_ON, 0);
}
