/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for BT scan module
 */
#ifndef __BT_SCAN_CONFIG_H__
#define __BT_SCAN_CONFIG_H__
//================================| Includes |================================
#if defined __has_include
#if __has_include("bt_scan_config_user.h")
#include "bt_scan_config_user.h"
#endif
#endif
// ================================| Defines |================================
// ============================| Default values |=============================
#ifndef BT_SCAN_DURATION_S
/**
 * @brief Single scan duration in seconds
 */
#define BT_SCAN_DURATION_S (5)
#endif  // BT_SCAN_DURATION_S

#ifndef BT_SCAN_PERIOD_S
/**
 * @brief Scan period in seconds
 *
 * This value does not take into account scan duration, so set it wisely.
 */
#define BT_SCAN_PERIOD_S (30)
#endif  // BT_SCAN_PERIOD_S

#ifndef BT_MIN_TX_PWR_DBM
/**
 * @brief Minimum transmit power for Bluetooth module in dBm
 *
 * Bluetooth stack will automatically change TX power in min/max range
 */
#define BT_MIN_TX_PWR_DBM (-12)
#endif  // BT_MIN_TX_PWR_DBM

#ifndef BT_MAX_TX_PWR_DBM
/**
 * @brief Maximum transmit power for Bluetooth module in dBm
 *
 * Bluetooth stack will automatically change TX power in min/max range
 */
#define BT_MAX_TX_PWR_DBM (-9)
#endif  // BT_MAX_TX_PWR_DBM

#ifndef BT_DROP_OFF_CNT
/**
 * @brief When device is not detected, try to scan X times before shutdown plug
 *
 * When reference device is not found, it might be caused by interference or
 * some bug at ESP BT module. Simply program should not turn off power plug
 * immediately, but perform few more scans to be sure. This value defines how
 * many extra scans should be done, before it is decided to finally shutdown
 * power plug
 */
#define BT_DROP_OFF_CNT (3)
#endif  // BT_DROP_OFF_CNT

#endif  // __BT_SCAN_CONFIG_H__
