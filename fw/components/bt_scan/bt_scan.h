/**
 * @file
 * @author Martin Stejskal
 * @brief Module responsible for BT scanning
 */
#ifndef __BT_SCAN_H__
#define __BT_SCAN_H__
//================================| Includes |================================
#include <esp_bt.h>
#include <stdint.h>
// ================================| Defines |================================
//===========================| Preprocessor checks |==========================
//============================| Structures, enums |===========================
//============================| Global variables |============================
//================================| Functions |===============================
/**
 * @brief Bluetooth scan task for RTOS
 * @param pvParameters Optional parameters. Can be actually empty.
 */
void btScanTaskRTOS(void *pvParameters);
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Convert input power in dBm into enumeration used by BT core
 *
 * @param iRequiredPwrDbm Required TX power in dBm
 * @param[out] pi8ActualPwrDbm Here will be stored real TX power. Point is that
 *                             in some cases required power can be out of
 *                             range, so it is better to know real status
 * @return Power as enumeration used by Bluetooth core
 */
esp_power_level_t btScanTxPwrIntToEnum(int8_t iRequiredPwrDbm,
                                       int8_t *pi8ActualPwrDbm);

#endif  // __BT_SCAN_H__
