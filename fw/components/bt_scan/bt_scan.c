/**
 * @file
 * @author Martin Stejskal
 * @brief Module responsible for BT scanning
 */
//================================| Includes |================================
// Project specific
#include "bt_scan.h"

#include "bt_scan_config.h"

// Work with non-volatile-storage
#include "nvs_settings.h"

// Power switch
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "mac.h"
#include "pwr_switch.h"
// ESP specific libraries
#include <esp_bt_device.h>
#include <esp_bt_main.h>
#include <esp_gap_bt_api.h>
#include <esp_log.h>
#include <esp_system.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// ================================| Defines |================================
#define BT_SETUP_TAG "BT setup"
#define BT_SCAN_TASK_TAG "BT scan tsk"
#define BT_CB_TAG "BT callback"
#define BT_API "BT API"

#define BT_GET_CLK_MS() (esp_log_timestamp())

#ifndef false
#define false 0
#endif
#ifndef true
#define true 1
#endif
// ============================| Default values |=============================
//===========================| Preprocessor checks |==========================
#if BT_SCAN_PERIOD_S <= BT_SCAN_DURATION_S
#error "Scan period can not be lower than scan duration"
#endif
//============================| Structures, enums |===========================
typedef enum { STATE_SCANNING, STATE_SCAN_DONE } t_states;
//============================| Global variables |============================
/**
 * @brief State machine variable
 */
static t_states geState = STATE_SCAN_DONE;

/**
 * @brief Was device found?
 */
static uint8_t gu8devFound = false;

/**
 * @brief How many times was not device found since last
 *
 * This counter is useful for delayed power off. Sometimes device might not be
 * discovered (due to various reasons) and power plug would be disabled
 * immediately. Not best behavior. So there is this counter which increasing
 * when no device is found and when reach threshold, power plug is powered off
 */
static uint8_t gu8devNotFoundCnt = 0;
//================================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief Convert BT MAC into string
 *
 * @param bda Byte array with BT MAC in raw format
 * @param str Buffer for output string
 * @param size Size of string buffer
 * @return Pointer to processed string. Technically it should be same as input
 * pointer to string buffer
 */
static char *_bda2str(esp_bd_addr_t bda, char *str, size_t size);

/**
 * @brief Print details of found Bluetooth device
 * @param param Pointer to device description structure
 */
static void _showDeviceInfo(esp_bt_gap_cb_param_t *param);

/**
 * @brief Callback for Bluetooth events
 * @param event Event type
 * @param param Additional informations
 */
static void _btAppGapCb(esp_bt_gap_cb_event_t event,
                        esp_bt_gap_cb_param_t *param);

/**
 * @brief Setup Bluetooth module
 * @param i8minTxPwrDbm Minimum TX power level in dBm
 * @param i8maxTxPwrDbm Maximum TX power level in dBm
 */
static void _setupBT(int8_t i8minTxPwrDbm, int8_t i8maxTxPwrDbm);

/**
 * @brief Check if found device matches with any registered device
 * @param au8foundMac Found MAC by Bluetooth
 * @return True if MAC match with any registered device, false otherwise
 */
static bool _wasDeviceFound(uint8_t au8foundMac[static MAC_SIZE_BYTES]);
// =========================| High level functions
// |==========================
void btScanTaskRTOS(void *pvParameters) {
  uint16_t u16scanPeriod;
  uint8_t u8scanDuration;
  int8_t i8minTxPwrDbm, i8maxTxPwrDbm;

  // Load scan period
  teNvsSttngsErr eErrCodeNvs =
      nvsSttngsGetScanPeriod(&u16scanPeriod, BT_SCAN_PERIOD_S);
  // Load scan duration
  eErrCodeNvs |= nvsSttngsGetScanDuration(&u8scanDuration, BT_SCAN_DURATION_S);
  // Load minimum TX power
  eErrCodeNvs |= nvsSttngsGetMinTxPwr(&i8minTxPwrDbm, BT_MIN_TX_PWR_DBM);
  // Load maximum TX power
  eErrCodeNvs |= nvsSttngsGetMaxTxPwr(&i8maxTxPwrDbm, BT_MAX_TX_PWR_DBM);

  if (eErrCodeNvs) {
    ESP_LOGE(BT_SCAN_TASK_TAG, "Loading settings from memory failed");
    return;
  }

  // Reset "not found" counter
  gu8devNotFoundCnt = 0;

  // Set state machine
  geState = STATE_SCAN_DONE;

  _setupBT(i8minTxPwrDbm, i8maxTxPwrDbm);

  for (;;) {
    /* start to discover nearby Bluetooth devices */
    if (geState == STATE_SCAN_DONE) {
      geState = STATE_SCANNING;
      gu8devFound = false;

      esp_bt_gap_start_discovery(ESP_BT_INQ_MODE_GENERAL_INQUIRY,
                                 u8scanDuration, 0);

      // Not scan too often
      vTaskDelay(u16scanPeriod * 1000 / portTICK_PERIOD_MS);
    }
  }
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
esp_power_level_t btScanTxPwrIntToEnum(int8_t iRequiredPwrDbm,
                                       int8_t *pi8ActualPwrDbm) {
  // Lowest power is -12 dBm (ESP_PWR_LVL_N12 -> check documentation)
  int8_t i8powerDbm = -12;
  // First set actual power. Step is 3 dBm -> -12 ~ +9 -> 8 values
  esp_power_level_t ePwr;
  for (ePwr = ESP_PWR_LVL_N12; ePwr <= ESP_PWR_LVL_P9; ePwr++) {
    if (iRequiredPwrDbm <= i8powerDbm) {
      if (iRequiredPwrDbm != i8powerDbm) {
        // Differs -> need to update & warn
        ESP_LOGW(BT_API, "%s TX power have to be changed to %d dBm", __func__,
                 i8powerDbm);
      }
      *pi8ActualPwrDbm = i8powerDbm;
      ESP_LOGI(BT_API, "Settings BT TX power to %d dBm", *pi8ActualPwrDbm);
      return ePwr;
    }
    // When continue, increment by 3 (step)
    i8powerDbm += 3;
  }
  // If goes there, value is higher than maximum -> handle it. Last time
  // power was increased by 3, but we need to lower it back to get last
  // valid value
  *pi8ActualPwrDbm = i8powerDbm - 3;
  ESP_LOGW(BT_API, "%s TX power is too high. Setting %d dBm", __func__,
           *pi8ActualPwrDbm);
  return ePwr;
}
// ==========================| Internal functions |===========================
static char *_bda2str(esp_bd_addr_t bda, char *str, size_t size) {
  if (bda == NULL || str == NULL || size < 18) {
    return NULL;
  }

  uint8_t *p = bda;
  sprintf(str, "%02x:%02x:%02x:%02x:%02x:%02x", p[0], p[1], p[2], p[3], p[4],
          p[5]);
  return str;
}

static void _showDeviceInfo(esp_bt_gap_cb_param_t *param) {
  char bda_str[18];
  uint32_t cod = 0;
  int32_t rssi = -129; /* invalid value */
  esp_bt_gap_dev_prop_t *p;

  ESP_LOGI(BT_CB_TAG, "Device found: %s",
           _bda2str(param->disc_res.bda, bda_str, sizeof(bda_str)));

  for (int i = 0; i < param->disc_res.num_prop; i++) {
    p = param->disc_res.prop + i;
    switch (p->type) {
      case ESP_BT_GAP_DEV_PROP_COD:
        cod = *(uint32_t *)(p->val);
        ESP_LOGI(BT_CB_TAG, "--Class of Device: 0x%x", cod);
        break;
      case ESP_BT_GAP_DEV_PROP_RSSI:
        rssi = *(int8_t *)(p->val);
        ESP_LOGI(BT_CB_TAG, "--RSSI: %d", rssi);
        break;
      case ESP_BT_GAP_DEV_PROP_BDNAME:
      default:
        break;
    }
  }
}

static void _btAppGapCb(esp_bt_gap_cb_event_t event,
                        esp_bt_gap_cb_param_t *param) {
  // Keep timestamp when device was not found. Sometimes ESP send multiple
  // events - need to consider timing too
  static uint64_t u64timeLastNotFoundMS = 0;

  switch (event) {
    case ESP_BT_GAP_DISC_RES_EVT: {
      ESP_LOGI(BT_CB_TAG, "ESP_BT_GAP_DISC_RES_EVT");
      _showDeviceInfo(param);

      if (_wasDeviceFound(param->disc_res.bda)) {
        // Device found
        ESP_LOGI(BT_CB_TAG, "  > Device found. Enabling power plug...");
        gu8devFound = true;
        // Reset "not found" counter
        gu8devNotFoundCnt = 0;

        // Enable power switch
        pwrSwBtSet(1);

        // No need to continue scanning
        esp_bt_gap_cancel_discovery();
      }

      break;
    }
    case ESP_BT_GAP_DISC_STATE_CHANGED_EVT: {
      ESP_LOGI(BT_CB_TAG, "ESP_BT_GAP_DISC_STATE_CHANGED_EVT");
      if (param->disc_st_chg.state == ESP_BT_GAP_DISCOVERY_STOPPED) {
        geState = STATE_SCAN_DONE;
        ESP_LOGI(BT_CB_TAG, "Discovery stopped. Device found: %u", gu8devFound);

        // If scan completed, but device not found -> power off
        if (!gu8devFound) {
          uint64_t u64timeActualMS = BT_GET_CLK_MS();

          ESP_LOGD(BT_CB_TAG, "Prev not found: %llu ms ; actual ms: %llu",
                   u64timeLastNotFoundMS, u64timeActualMS);
          // Update at least once per 1s
          if ((u64timeActualMS - u64timeLastNotFoundMS) < 1000) {
            // Less than 1s -> no process
            u64timeLastNotFoundMS = u64timeActualMS;
            break;
          } else {
            // Store timestamp
            u64timeLastNotFoundMS = u64timeActualMS;
          }

          uint8_t u8dropOffCnt;
          nvsSttngsGetDropOffCnt(&u8dropOffCnt, BT_DROP_OFF_CNT);

          ESP_LOGD(BT_CB_TAG, "Dev not found cnt: %u ; drop off cnt: %u",
                   gu8devNotFoundCnt, u8dropOffCnt);

          if (gu8devNotFoundCnt >= u8dropOffCnt) {
            ESP_LOGI(BT_CB_TAG, "  > Device NOT found. Powering off...");
            pwrSwBtSet(0);
          } else {
            // Device not found, but there is still chance
            gu8devNotFoundCnt++;
          }
        }
      }
      break;
    }
    case ESP_BT_GAP_RMT_SRVCS_EVT: {
      ESP_LOGI(BT_CB_TAG, "ESP_BT_GAP_RMT_SRVCS_EVT");
      break;
    }
    case ESP_BT_GAP_RMT_SRVC_REC_EVT:
    default: {
      ESP_LOGI(BT_CB_TAG, "event: %d", event);
      break;
    }
  }
}

static void _setupBT(int8_t i8minTxPwrDbm, int8_t i8maxTxPwrDbm) {
  /* ESP32 using FreeRTOS and it is expected that this function is there
   * mainly "just for settings". If some loop-style function should run on
   * MCU, new task should be created here.
   */
  ESP_LOGI(BT_SETUP_TAG, "Configuring BT...");
  esp_err_t ret;
  int8_t i8dummy;

  ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_BLE));

  // Check if BT is enabled in menuconfig
  esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();

  if ((ret = esp_bt_controller_init(&bt_cfg)) != ESP_OK) {
    ESP_LOGE(BT_SETUP_TAG, "%s initialize controller failed: %s\n", __func__,
             esp_err_to_name(ret));
    return;
  }

  // Some "classic" mode
  if ((ret = esp_bt_controller_enable(ESP_BT_MODE_CLASSIC_BT)) != ESP_OK) {
    ESP_LOGE(BT_SETUP_TAG, "%s enable controller failed: %s\n", __func__,
             esp_err_to_name(ret));
    return;
  }

  // Set TX power range
  if ((ret = esp_bredr_tx_power_set(
           btScanTxPwrIntToEnum(i8minTxPwrDbm, &i8dummy),
           btScanTxPwrIntToEnum(i8maxTxPwrDbm, &i8dummy))) != ESP_OK) {
    ESP_LOGE(BT_SETUP_TAG, "%s settings TX power failed: %s\n", __func__,
             esp_err_to_name(ret));
    return;
  }

  // Init and allocate resources for BT
  if ((ret = esp_bluedroid_init()) != ESP_OK) {
    ESP_LOGE(BT_SETUP_TAG, "%s initialize bluedroid failed: %s\n", __func__,
             esp_err_to_name(ret));
    return;
  }

  // Enable BT (finally)
  if ((ret = esp_bluedroid_enable()) != ESP_OK) {
    ESP_LOGE(BT_SETUP_TAG, "%s enable bluedroid failed: %s\n", __func__,
             esp_err_to_name(ret));
    return;
  }

  char *dev_name = "Smart plug";
  esp_bt_dev_set_device_name(dev_name);

  /* set discoverable and connectable mode, wait to be connected */
  // By using mode "none", all devices suppose to be visible
  esp_bt_gap_set_scan_mode(ESP_BT_NON_CONNECTABLE, ESP_BT_NON_DISCOVERABLE);

  /* register GAP callback function */
  esp_bt_gap_register_callback(_btAppGapCb);

  ESP_LOGI(BT_SETUP_TAG, "[done]\n");
}

static bool _wasDeviceFound(uint8_t au8foundMac[static MAC_SIZE_BYTES]) {
  static uint8_t au8macList[NVS_MAX_REGISTERED_MAC][MAC_SIZE_BYTES];
  bool bDeviceFound = false;

  teNvsSttngsErr eErrCode = nvsSttngsGetBtMacList(au8macList);
  if (eErrCode) {
    ESP_LOGE(BT_CB_TAG, "Can not read from NVS!");
  } else {
    // Read was successful -> go through MAC list and keep comparing
    for (uint16_t u16macIdx = 0; u16macIdx < NVS_MAX_REGISTERED_MAC;
         u16macIdx++) {
      // If MAC in list is not empty, check if match
      if (!macIsEmpty(au8macList[u16macIdx])) {
        // OK, so MAC in predefined list is not empty, so compare them
        if (areMacSame(au8macList[u16macIdx], au8foundMac)) {
          bDeviceFound = true;
          break;
        }  // if MAC match
      }    // if MAC in list is not empty
    }      // For all records in MAC list
  }        // Can read from NVS

  return bDeviceFound;
}
