/**
 * @file
 * @author Martin Stejskal
 * @brief Library that deal with MAC functions
 */
#ifndef __MAC__H__
#define __MAC__H__
// ===============================| Includes |================================
// Standard
#include <stdbool.h>
#include <stdint.h>

// ESP specific
#include <esp_err.h>
// ================================| Defines |================================
/**
 * @brief Define MAC size in Bytes
 */
#define MAC_SIZE_BYTES (6)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Convert MAC in binary format to string
 * @param au8mac Pointer to 8 bit data array
 * @return Pointer to string where result is stored
 *
 * @note Until function is called again, data are not overwritten
 */
char *macBinToStr(uint8_t au8mac[static MAC_SIZE_BYTES]);

/**
 * @brief Convert string to array of bytes
 * @param pacStr Pointer to string
 * @param au8mac Pointer to Byte array where MAC will be written
 * @return ESP_OK if no problem is detected
 */
esp_err_t macStrToBin(char *pacStr, uint8_t au8mac[static MAC_SIZE_BYTES]);

/**
 * @brief Returns true if MAC are same, false otherwise
 * @param au8mac1 MAC 1
 * @param au8mac2 MAC 2
 * @return True if MAC 1 and 2 are equal. False otherwise
 */
bool areMacSame(const uint8_t au8mac1[static MAC_SIZE_BYTES],
                const uint8_t au8mac2[static MAC_SIZE_BYTES]);

/**
 * @brief Check if MAC is empty or not
 * @param au8mac MAC in binary format
 * @return True if empty, false otherwise
 */
bool macIsEmpty(uint8_t au8mac[static MAC_SIZE_BYTES]);

// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __MAC__H__
