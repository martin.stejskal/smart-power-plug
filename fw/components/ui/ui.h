/**
 * @file
 * @author Martin Stejskal
 * @brief Simple user interface for Smart Power Plug
 */
#ifndef __UI_H__
#define __UI_H__
//================================| Includes |================================
// ================================| Defines |================================
// ============================| Default values |=============================
//===========================| Preprocessor checks |==========================
//============================| Structures, enums |===========================
typedef enum {
  UI_INIT,
  UI_WAIT_FOR_INPUT,
  UI_PROCESS_CMD,

  UI_INTERNAL_ERROR
} uiStates_t;

typedef enum {
  UI_OK = 0,
  UI_ERROR,
  UI_INVALID_MAC,
  UI_UART_ERR,
  UI_UART_CONFIG_ERR,
  UI_UART_IO_ERR,
  UI_NO_MEM_ERR,
  UI_NVM_ERR,
  UI_PRINT_ERR,
  UI_CONVERSION_ERR,
} uiErr_t;
//================================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief UI task for RTOS
 *
 * When RTOS system is used, this task suppose to run
 *
 * @param pvParameters Pointer to parameters
 */
void uiTaskRTOS(void *pvParameters);

/**
 * @brief UI cooperative task
 *
 * When RTOS is not available, this function suppose to be called periodically
 * or at least "often" enough to handle user interaction
 */
void uiTask(void);
// ========================| Middle level functions |=========================
/**
 * @brief Initialize UI module
 *
 * When this is "first time", it will automatically print help.
 *
 * @param pStatePrevious Pointer to previous state
 * @param pStateActual Pointer to current state
 */
void uiInit(uiStates_t *pStatePrevious, uiStates_t *pStateActual);

/**
 * @brief Waits for user to finish input
 *
 * Typically it waits for "enter" key. It also deals with backspace and deleting
 * previously typed characters
 *
 *
 * @param pStatePrevious Pointer to previous state
 * @param pStateActual Pointer to actual state
 * @param pacUserInputBuff Pointer to user input buffer
 * @param piUserInputBuffWriteIdx Pointer to current index at user input buffer
 */
void uiWaitForInput(uiStates_t *pStatePrevious, uiStates_t *pStateActual,
                    char *pacUserInputBuff, int *piUserInputBuffWriteIdx);

/**
 * @brief Process given command if possible
 *
 * Assume that user input is finished and check if that input matches with some
 * command. If so, it will call appropriate function.
 *
 * @param pStatePrevious Pointer to previous state
 * @param pStateActual Pointer to actual state
 * @param pacUserInputBuff Pointer to user input buffer
 * @param piUserInputBuffWriteIdx Pointer to current index at user input buffer
 */
void uiProcessCmd(uiStates_t *pStatePrevious, uiStates_t *pStateActual,
                  char *pacUserInputBuff, int *piUserInputBuffWriteIdx);
// ==========================| Low level functions |==========================
#endif  // __UI_H__
