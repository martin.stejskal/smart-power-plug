/**
 * @file
 * @author Martin Stejskal
 * @brief Simple user interface for Smart Power Plug
 */
//================================| Includes |================================
#include "ui.h"

#include "ui_config.h"
#include "ui_msgs.c"

// Work with non-volatile-storage
#include "nvs_settings.h"

// Some supporting functions regarding to the MAC
#include "mac.h"

// Standard ESP32 libraries
#include <driver/uart.h>
#include <esp_log.h>
#include <string.h>
// ================================| Defines |================================
/**
 * @brief Macro which print string over UART peripheral
 *
 * This is kind of forwarding macro. Simple, but very effective
 */
#define _uiPrintStr(str) uart_write_bytes(UI_UART_INTERFACE, str, strlen(str))

/**
 * @brief Raw prints to UART
 */
#define _uiPrintRaw(data, len) uart_write_bytes(UI_UART_INTERFACE, data, len)

/**
 * @brief If detected error, exit function immediately
 */
#define UI_CHECK_PRINT_ERR_CODE(eErr)            \
  if (eErr == -1) {                              \
    ESP_LOGE(UI_TAG, "%s", psCanNotPrintToUart); \
    return UI_PRINT_ERR;                         \
  }
/**
 * @brief Define RF IC TX power capability
 *
 * This is HW limitation - should not be changed or modified. Please refer to
 * esp_power_level_t enum.
 *
 * @{
 */
#define UI_RF_IC_MIN_TX_DBM (-12)
#define UI_RF_IC_MAX_TX_DBM (9)
/**
 * @}
 */

// ============================| Default values |=============================
//===========================| Preprocessor checks |==========================
//============================| Structures, enums |===========================
//============================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *UI_TAG = "UI";

// Was driver already installed?
static uint8_t gu8uartDriverInstalled = 0;
//================================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief Commands which user can call over service UI
 *
 * @{
 */
static void _cmdGetBtMacList(void);
static void _cmdSetBtMac(char *pacUserInputBuff);
static void _cmdClearMacList(void);
static void _cmdGetScanDuration(void);
static void _cmdSetScanDuration(char *pacUserInputBuff);
static void _cmdGetScanPeriod(void);
static void _cmdSetScanPeriod(char *pacUserInputBuff);
static void _cmdGetMinTxPwr(void);
static void _cmdSetMinTxPwr(char *pacUserInputBuff);
static void _cmdGetMaxTxPwr(void);
static void _cmdSetMaxTxPwr(char *pacUserInputBuff);
static void _cmdGetDropOffCnt(void);
static void _cmdSetDropOffCnt(char *pacUserInputBuff);
static void _cmdGetExtLoad(void);
static void _cmdSetExtLoad(char *pacUserInputBuff);
static void _cmdDefaultSettings(void);
/**
 * @}
 */

/**
 * @brief Initialize UART interface through which user will interact
 * @return Zero if everything is fine, otherwise there is error
 */
static uiErr_t _initUART(void);

/**
 * @brief Convert string to array of bytes
 * @param pacStr Pointer to string
 * @param au8mac Pointer to Byte array where MAC will be written
 * @return ESP_OK if no problem is detected
 */
static uiErr_t _parseMac(char *pacStr, uint8_t au8mac[MAC_SIZE_BYTES]);

/**
 * @brief Append data from source buffer to destination buffer
 *
 * @param pacSrc Pointer to source buffer
 * @param iSrcSize Number of Bytes which will be copied to destination buffer
 * @param pacDst Pointer to destination buffer
 * @param piDstWriteIdx Actual write index for destination buffer. From this
 *                      index will be written data from source buffer.
 * @param iDstBuffSize Total size of destination buffer. This will help to
 *                     detect potential buffer overflow.
 * @return Non zero value if there is problem.
 */
static uiErr_t _appendBuffer(const char *pacSrc, const int iSrcSize,
                             char *pacDst, int *piDstWriteIdx,
                             const int iDstBuffSize);

/**
 * @brief Load BT MAC from NVS
 * @param au8mac Pointer to data array where loaded MAC will be written
 * @return UI_OK if no error
 */
static uiErr_t _getMacFromNVS(
    uint8_t au8mac[NVS_MAX_REGISTERED_MAC][MAC_SIZE_BYTES]);

/**
 * @brief Check error code and print error message or reset system
 * @param eNvsErr Error code returned by some nvs_settings function
 */
static void _checkNvsErrCode(teNvsSttngsErr eNvsErr);

/**
 * @brief Find new line or carriage return in buffer
 * @param pacBuff Pointer to buffer with characters
 * @param iWriteIdx Write index - up to this index will be searching.
 * @return Index where new line or carriage return was found
 */
static int _findNewLine(const char *pacBuff, const int iWriteIdx);

/**
 * @brief Convert 2 bytes from string into integer
 * @param pacStr Pointer to string with 2 Bytes of HEX value as string
 * @param piResult Converted value will be written there
 * @return UI_OK if no error
 */
static uiErr_t _StrHex2ByteToInt(const char *pacStr, uint8_t *piResult);

/**
 * @brief Convert character ASCII HEX value to integer
 * @param cHex HEX value in ASCII
 * @param piResult Result will be written here
 * @return UI_OK if no error
 */
static uiErr_t _charHexToInt(const char cHex, uint8_t *piResult);

/**
 * @brief Set of functions that prints some information to user
 *
 * @{
 */
static uiErr_t _printHelp(void);
static uiErr_t _printMac(uint8_t au8mac[MAC_SIZE_BYTES]);
static uiErr_t _printInvalidMacInput(void);
static uiErr_t _printInvalidScanDurationInput(void);
static uiErr_t _printInvalidScanPeriodInput(void);
static uiErr_t _printInvalidMinTxPwrInput(void);
static uiErr_t _printMinTxPwrHigerThanMaxTx(void);
static uiErr_t _printInvalidMaxTxPwrInput(void);
static uiErr_t _printMaxTxPwrLowerThanMinTx(void);
static uiErr_t _printInvalidDropOffCntInput(void);
static uiErr_t _printInvalidExtLoadInput(void);
static uiErr_t _printNoFreeSlotForNewMac(void);
/**
 * @}
 */

/**
 * @brief Set additional current consumption
 *
 * This might be needed in order to get rid of annoying pitch sound from
 * AC/DC converter.
 *
 * @param u8loadLevel Load level. 0 - no additional load, 4 - maximum additional
 * load
 */
static void _setExtraLoadLevel(uint8_t u8loadLevel);
// =========================| High level functions |==========================

void uiTaskRTOS(void *pvParameters) {
  // Load "extra load" value and apply it
  uint8_t u8extraLoadLevel;
  nvsSttngsGetExtraLoad(&u8extraLoadLevel, UI_DEFAULT_EXTRA_PWR_LOAD_LVL);

  _setExtraLoadLevel(u8extraLoadLevel);

  for (;;) {
    uiTask();
    vTaskDelay(UI_TASK_PERIOD_MS / portTICK_PERIOD_MS);
  }
}

void uiTask(void) {
  static uiStates_t eStateActual, eStatePrevious = UI_INIT;

  // User input buffer for complete user input
  static char acUserInputBuff[UI_RX_BUFFER_SIZE];
  // Write index for user input buffer
  static int iUserInputBuffWriteIdx;

  switch (eStateActual) {
    case UI_INIT:
      uiInit(&eStatePrevious, &eStateActual);
      break;
    case UI_WAIT_FOR_INPUT:
      uiWaitForInput(&eStatePrevious, &eStateActual, acUserInputBuff,
                     &iUserInputBuffWriteIdx);
      break;
    case UI_PROCESS_CMD:
      uiProcessCmd(&eStatePrevious, &eStateActual, acUserInputBuff,
                   &iUserInputBuffWriteIdx);
      break;
    case UI_INTERNAL_ERROR:
      return;
      break;
    default:
      ESP_LOGE(UI_TAG, "Internal error! Unknown state: %d", eStateActual);
  }
}
// ========================| Middle level functions |=========================

void uiInit(uiStates_t *pStatePrevious, uiStates_t *pStateActual) {
  *pStatePrevious = *pStateActual;

  if (_initUART()) {
    *pStateActual = UI_INTERNAL_ERROR;
  } else {
    // UART init pass -> print help
    if (_printHelp()) {
      *pStateActual = UI_INTERNAL_ERROR;
    } else {
      // Otherwise wait for user input
      *pStateActual = UI_WAIT_FOR_INPUT;
    }
  }
}

void uiWaitForInput(uiStates_t *pStatePrevious, uiStates_t *pStateActual,
                    char *pacUserInputBuff, int *piUserInputBuffWriteIdx) {
  int iNewLineIdx = 0;

  // If previous state was processing command, clear buffers
  if ((*pStatePrevious == UI_PROCESS_CMD) || (*pStatePrevious == UI_INIT)) {
    *piUserInputBuffWriteIdx = 0;
    _uiPrintStr("\n$ ");
  }

  *pStatePrevious = *pStateActual;

  // Temporary buffer
  char acRxBytes[UI_RX_BUFFER_SIZE] = {0};

  int iRxBytesCnt =
      uart_read_bytes(UI_UART_INTERFACE, (uint8_t *)acRxBytes,
                      sizeof(acRxBytes), UI_RX_USER_INPUT_TIMEOUT_MS);
  // If nothing to process, exit preliminary
  if (iRxBytesCnt == 0) {
    return;
  }
  ///@todo Solve "problem" with deleting "$ " when pushing backspace
  // Print input back to user to have some feedback
  _uiPrintRaw(acRxBytes, iRxBytesCnt);

  ESP_LOGV(UI_TAG, "RX %d user chars (Widx: %d)", iRxBytesCnt,
           *piUserInputBuffWriteIdx);

  // Put data to user input buffer
  uiErr_t eErrCode = _appendBuffer(acRxBytes, iRxBytesCnt, pacUserInputBuff,
                                   piUserInputBuffWriteIdx, UI_RX_BUFFER_SIZE);
  if (eErrCode) {
    // Something went wrong -> re-initialize
    ESP_LOGE(UI_TAG, "Error when adding user input to input buffer");
    *pStateActual = UI_INIT;
  }

  for (int i = 0; i < *piUserInputBuffWriteIdx; i++) {
    ESP_LOGV(UI_TAG, " > %c (%d)", pacUserInputBuff[i],
             (int)pacUserInputBuff[i]);
  }

  // Search for new line
  iNewLineIdx = _findNewLine(pacUserInputBuff, *piUserInputBuffWriteIdx);
  if (iNewLineIdx >= 0) {
    // New line found -> put NULL -> process as string
    pacUserInputBuff[iNewLineIdx] = 0x00;

    *pStateActual = UI_PROCESS_CMD;

    // Write new line, so new written text will not be overwritten
    _uiPrintStr("\n");
  }
}

void uiProcessCmd(uiStates_t *pStatePrevious, uiStates_t *pStateActual,
                  char *pacUserInputBuff, int *piUserInputBuffWriteIdx) {
  *pStatePrevious = *pStateActual;

  ESP_LOGD(UI_TAG, "Received command: %s", pacUserInputBuff);

  // Process command
  if (strcmp(UI_BT_MAC_LIST_GET_CMD, pacUserInputBuff) == 0) {
    // Get MAC command
    _cmdGetBtMacList();
  } else if (memcmp(UI_BT_MAC_SET_CMD, pacUserInputBuff,
                    sizeof(UI_BT_MAC_SET_CMD) - 1) == 0) {
    // Set MAC command
    _cmdSetBtMac(pacUserInputBuff);

  } else if (strcmp(UI_BT_MAC_LIST_DEL_CMD, pacUserInputBuff) == 0) {
    _cmdClearMacList();

  } else if (strcmp(UI_SCAN_DUR_GET_CMD, pacUserInputBuff) == 0) {
    // Get scan duration
    _cmdGetScanDuration();

  } else if (memcmp(UI_SCAN_DUR_SET_CMD, pacUserInputBuff,
                    sizeof(UI_SCAN_DUR_SET_CMD) - 1) == 0) {
    // Set scan duration
    _cmdSetScanDuration(pacUserInputBuff);

  } else if (strcmp(UI_SCAN_PERIOD_GET_CMD, pacUserInputBuff) == 0) {
    // Get scan period
    _cmdGetScanPeriod();

  } else if (memcmp(UI_SCAN_PERIOD_SET_CMD, pacUserInputBuff,
                    sizeof(UI_SCAN_PERIOD_SET_CMD) - 1) == 0) {
    // Set scan period
    _cmdSetScanPeriod(pacUserInputBuff);

  } else if (strcmp(UI_MIN_TX_PWR_GET_CMD, pacUserInputBuff) == 0) {
    // Get minimum TX power
    _cmdGetMinTxPwr();

  } else if (memcmp(UI_MIN_TX_PWR_SET_CMD, pacUserInputBuff,
                    sizeof(UI_MIN_TX_PWR_SET_CMD) - 1) == 0) {
    // Set minimum TX power
    _cmdSetMinTxPwr(pacUserInputBuff);

  } else if (strcmp(UI_MAX_TX_PWR_GET_CMD, pacUserInputBuff) == 0) {
    // Get maximum TX power
    _cmdGetMaxTxPwr();

  } else if (memcmp(UI_MAX_TX_PWR_SET_CMD, pacUserInputBuff,
                    sizeof(UI_MAX_TX_PWR_SET_CMD) - 1) == 0) {
    // Set maximum TX power
    _cmdSetMaxTxPwr(pacUserInputBuff);

  } else if (strcmp(UI_DROP_OFF_GET_CMD, pacUserInputBuff) == 0) {
    // Get drop off counter
    _cmdGetDropOffCnt();

  } else if (memcmp(UI_DROP_OFF_SET_CMD, pacUserInputBuff,
                    sizeof(UI_DROP_OFF_SET_CMD) - 1) == 0) {
    // Set drop off counter
    _cmdSetDropOffCnt(pacUserInputBuff);
  } else if (strcmp(UI_EXT_LOAD_GET_CMD, pacUserInputBuff) == 0) {
    _cmdGetExtLoad();

  } else if (memcmp(UI_EXT_LOAD_SET_CMD, pacUserInputBuff,
                    sizeof(UI_EXT_LOAD_SET_CMD) - 1) == 0) {
    _cmdSetExtLoad(pacUserInputBuff);

  } else if (strcmp(UI_DEFAULT_CMD, pacUserInputBuff) == 0) {
    // Load default settings
    _cmdDefaultSettings();

  } else {
    ESP_LOGD(UI_TAG, "Command not recognized");
    _printHelp();
  }

  // Go back to wait for user input
  *pStateActual = UI_WAIT_FOR_INPUT;
  *piUserInputBuffWriteIdx = 0;
}
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================

//================================| Commands |=================================
static void _cmdGetBtMacList(void) {
  uint8_t au8macList[NVS_MAX_REGISTERED_MAC][MAC_SIZE_BYTES];
  _getMacFromNVS(au8macList);

  for (uint16_t u16macIdx = 0; u16macIdx < NVS_MAX_REGISTERED_MAC;
       u16macIdx++) {
    if (macIsEmpty(au8macList[u16macIdx])) {
      // MAC is empty - do not print it
    } else {
      // MAC is set - print it
      _printMac(au8macList[u16macIdx]);
      _uiPrintStr("\n");
    }
  }
}

static void _cmdSetBtMac(char *pacUserInputBuff) {
  /* Note - sizeof() return size including NULL - do not want to compare
   * string with NULL -> reason why "-1" is there
   */
  // List of all stored MAC in NVS
  uint8_t au8macList[NVS_MAX_REGISTERED_MAC][MAC_SIZE_BYTES];

  if (nvsSttngsGetBtMacList(au8macList)) {
    ESP_LOGE(UI_TAG, "Can not read from NVS!");
    _uiPrintStr("Can not read from NVS!");
    return;
  }

  // User defined new MAC
  uint8_t au8newMac[MAC_SIZE_BYTES];

  // Will be used later. Need to check if free slot is available
  bool bFoundFreeSlot = false;
  uint16_t u16slotIdx;

  // Parse MAC - skip command part and go directly to MAC
  uiErr_t eErrCode =
      _parseMac(&pacUserInputBuff[sizeof(UI_BT_MAC_SET_CMD) - 1], au8newMac);
  if (eErrCode == UI_OK) {
    // Find free slot and write MAC there
    // Otherwise it is not in list yet. Find free slot
    for (u16slotIdx = 0; u16slotIdx < NVS_MAX_REGISTERED_MAC; u16slotIdx++) {
      if (macIsEmpty(au8macList[u16slotIdx])) {
        bFoundFreeSlot = true;
        break;
      }
    }

    if (bFoundFreeSlot) {
      // Write given MAC there
      memcpy(au8macList[u16slotIdx], au8newMac, sizeof(au8newMac));

      // Try to write to NVS and process
      _checkNvsErrCode(nvsSttngsSetBtMacList(au8macList));
    } else {
      // Else MAC is valid, but not free slot found
      _printNoFreeSlotForNewMac();
    }
  } else {
    _printInvalidMacInput();
  }
}

static void _cmdClearMacList(void) { nvsSttngsDelMacList(); }

static void _cmdGetScanDuration(void) {
  uint8_t u8scanDuration;
  // For string - 3 digits, 1 space, 1 character, 1 new line + NULL
  char acScanDuration[7];
  // Do not care about error code, just load and show
  nvsSttngsGetScanDuration(&u8scanDuration, UI_DEFAULT_SCAN_DURATION_S);

  sprintf(acScanDuration, "%i s\n", u8scanDuration);
  _uiPrintStr(acScanDuration);
}

static void _cmdSetScanDuration(char *pacUserInputBuff) {
  // Convert string to number (decimal format expected)
  int iScanDuration =
      strtol(&pacUserInputBuff[sizeof(UI_SCAN_DUR_SET_CMD) - 1], 0, 10);
  // Check if conversion was successful or not. Also re-check range
  if ((iScanDuration <= 0) || (iScanDuration > 255)) {
    _printInvalidScanDurationInput();
    return;
  }

  // Write to NVS
  _checkNvsErrCode(nvsSttngsSetScanDuration((uint8_t)iScanDuration));
}

static void _cmdGetScanPeriod(void) {
  uint16_t u16scanPeriod;
  // For string - 5 digits, 1 space, 1 character, 1 new line + NULL
  char acScanPeriod[9];
  // Do not care about error code, just load and show
  nvsSttngsGetScanPeriod(&u16scanPeriod, UI_DEFAULT_SCAN_PERIOD_S);

  sprintf(acScanPeriod, "%i s\n", u16scanPeriod);
  _uiPrintStr(acScanPeriod);
}

static void _cmdSetScanPeriod(char *pacUserInputBuff) {
  // Convert string to number (decimal format expected)
  int iScanPeriod =
      strtol(&pacUserInputBuff[sizeof(UI_SCAN_PERIOD_SET_CMD) - 1], 0, 10);

  // Check if conversion was successful or not. Also re-check range
  if ((iScanPeriod <= 0) || (iScanPeriod > 65535)) {
    _printInvalidScanPeriodInput();
    return;
  }

  // Write to NVS
  _checkNvsErrCode(nvsSttngsSetScanPeriod((uint16_t)iScanPeriod));
}

static void _cmdGetMinTxPwr(void) {
  int8_t i8minPwrDbm;
  // 4 digits (-100 for example), 1 space, 3 characters, 1 new line + NULL
  char acMinTxPwrDbm[10];
  nvsSttngsGetMinTxPwr(&i8minPwrDbm, UI_DEFAULT_MIN_TX_PWR_DBM);

  sprintf(acMinTxPwrDbm, "%i dBm\n", i8minPwrDbm);
  _uiPrintStr(acMinTxPwrDbm);
}

static void _cmdSetMinTxPwr(char *pacUserInputBuff) {
  int iMinTxPwrDbm =
      strtol(&pacUserInputBuff[sizeof(UI_MIN_TX_PWR_SET_CMD) - 1], 0, 10);
  // Load maximum value as well - check it later. Minimum should not be
  // higher than set maximum
  int8_t i8MaxTwPwrDbm;
  nvsSttngsGetMaxTxPwr(&i8MaxTwPwrDbm, UI_DEFAULT_MAX_TX_PWR_DBM);

  // Check if conversion was successful or not. And check range
  if ((iMinTxPwrDbm < UI_RF_IC_MIN_TX_DBM) ||
      (iMinTxPwrDbm > UI_RF_IC_MAX_TX_DBM)) {
    _printInvalidMinTxPwrInput();
    return;
  }

  // Check if minimum is not higher than maximum
  if (iMinTxPwrDbm > i8MaxTwPwrDbm) {
    _printMinTxPwrHigerThanMaxTx();
    return;
  }

  // Write to NVS
  _checkNvsErrCode(nvsSttngsSetMinTxPwr((int8_t)iMinTxPwrDbm));
}

static void _cmdGetMaxTxPwr(void) {
  int8_t i8maxPwrDbm;
  // 4 digits (-100 for example), 1 space, 3 characters, 1 new line + NULL
  char acMaxTxPwrDbm[10];
  nvsSttngsGetMaxTxPwr(&i8maxPwrDbm, UI_DEFAULT_MAX_TX_PWR_DBM);

  sprintf(acMaxTxPwrDbm, "%i dBm\n", i8maxPwrDbm);
  _uiPrintStr(acMaxTxPwrDbm);
}

static void _cmdSetMaxTxPwr(char *pacUserInputBuff) {
  int iMaxTxPwrDbm =
      strtol(&pacUserInputBuff[sizeof(UI_MAX_TX_PWR_SET_CMD) - 1], 0, 10);
  // Load minimum value as well - check it later. Maximum should not be
  // lower than set minimum
  int8_t i8MinTwPwrDbm;
  nvsSttngsGetMinTxPwr(&i8MinTwPwrDbm, UI_DEFAULT_MIN_TX_PWR_DBM);

  // Check if conversion was successful or not. And check range
  if ((iMaxTxPwrDbm < UI_RF_IC_MIN_TX_DBM) ||
      (iMaxTxPwrDbm > UI_RF_IC_MAX_TX_DBM)) {
    _printInvalidMaxTxPwrInput();
    return;
  }

  // Check if minimum is not higher than maximum
  if (iMaxTxPwrDbm < i8MinTwPwrDbm) {
    _printMaxTxPwrLowerThanMinTx();
    return;
  }

  // Write to NVS
  _checkNvsErrCode(nvsSttngsSetMaxTxPwr((int8_t)iMaxTxPwrDbm));
}

static void _cmdGetDropOffCnt(void) {
  uint8_t u8dropOffCnt;
  // 3 digits (130 for example), 1 character, 1 new line + NULL
  char acDropOffCnt[6];
  nvsSttngsGetDropOffCnt(&u8dropOffCnt, UI_DEFAULT_DROP_OFF_CNT);

  sprintf(acDropOffCnt, "%ux\n", u8dropOffCnt);
  _uiPrintStr(acDropOffCnt);
}

static void _cmdSetDropOffCnt(char *pacUserInputBuff) {
  long lDropOffCnt =
      strtol(&pacUserInputBuff[sizeof(UI_DROP_OFF_SET_CMD) - 1], 0, 10);

  if ((lDropOffCnt < 0) || (lDropOffCnt > 255)) {
    _printInvalidDropOffCntInput();
    return;
  }

  // Write to NVS
  _checkNvsErrCode(nvsSttngsSetDropOffCnt((uint8_t)lDropOffCnt));
}

static void _cmdGetExtLoad(void) {
  // Load value as integer
  uint8_t u8extraLoadLevel;
  nvsSttngsGetExtraLoad(&u8extraLoadLevel, UI_DEFAULT_EXTRA_PWR_LOAD_LVL);

  // Convert it to string and print it. Limit is 8 bit -> 255 -> 3 characters
  // + new line + NULL -> 5
  char acExtraLoadLevel[5];
  sprintf(acExtraLoadLevel, "%u", u8extraLoadLevel);
  _uiPrintStr(acExtraLoadLevel);
}
static void _cmdSetExtLoad(char *pacUserInputBuff) {
  long lExtraLoadLevel =
      strtol(&pacUserInputBuff[sizeof(UI_EXT_LOAD_SET_CMD) - 1], 0, 10);

  if ((lExtraLoadLevel < 0) || (lExtraLoadLevel > 4)) {
    _printInvalidExtLoadInput();
    return;
  }

  // Write to NVS
  _checkNvsErrCode(nvsSttngsSetExtraLoad((uint8_t)lExtraLoadLevel));
}

static void _cmdDefaultSettings(void) {
  // Erase all settings - check error code at the end
  teNvsSttngsErr eErr = nvsSttngsDelMacList();
  eErr |= nvsSttngsDelScanDuration();
  eErr |= nvsSttngsDelScanPeriod();
  eErr |= nvsSttngsDelMinTxPwr();
  eErr |= nvsSttngsDelMaxTxPwr();
  eErr |= nvsSttngsDelDropOffCnt();
  eErr |= nvsSttngsDelExtraLoad();
  _checkNvsErrCode(eErr);
}
//========================| Other internal functions |=========================
static uiErr_t _initUART(void) {
  uiErr_t eErr;
  // =================================| UART |==============================
  const uart_config_t uart_config = {.baud_rate = UI_UART_BAUDRATE,
                                     .data_bits = UART_DATA_8_BITS,
                                     .parity = UART_PARITY_DISABLE,
                                     .stop_bits = UART_STOP_BITS_1,
                                     .flow_ctrl = UART_HW_FLOWCTRL_DISABLE};
  // Try to remove driver if already installed
  if (gu8uartDriverInstalled) {
    uart_driver_delete(UI_UART_INTERFACE);
  }

  eErr = uart_param_config(UI_UART_INTERFACE, &uart_config);
  if (eErr) {
    ESP_LOGE(UI_TAG, "UART configuration failed");
    return UI_UART_CONFIG_ERR;
  }

  eErr = uart_set_pin(UI_UART_INTERFACE, UI_TXD_PIN, UI_RXD_PIN,
                      UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
  if (eErr) {
    ESP_LOGE(UI_TAG, "UART pin set fail");
    return UI_UART_IO_ERR;
  }

  eErr =
      uart_driver_install(UI_UART_INTERFACE, UI_RX_BUFFER_SIZE, 0, 0, NULL, 0);
  if (eErr) {
    ESP_LOGE(UI_TAG, "UART driver install failed: %d", eErr);
    return UI_UART_ERR;
  } else {
    gu8uartDriverInstalled = 1;
  }
  return UI_OK;
}

static uiErr_t _parseMac(char *pacStr, uint8_t au8mac[6]) {
  uiErr_t eErr = UI_ERROR;
  ESP_LOGD(UI_TAG, " parse mac: %s (%d)", pacStr, strlen(pacStr));
  // Expected format: xx:xx:xx:xx:xx:xx -> 17 chars
  if (strlen(pacStr) != 17) {
    return ESP_ERR_INVALID_MAC;
  }

  // Expecting 6 Bytes split by ":"
  for (int iByteCnt = 0; iByteCnt < 6; iByteCnt++) {
    // Pass pointer to MAC array as "base address" + offset
    eErr = _StrHex2ByteToInt(pacStr, au8mac + iByteCnt);
    if (eErr) {
      return eErr;
    }
    // And move string pointer by 3 (format is "xx:")
    pacStr += 3;
  }

  return eErr;
}

static uiErr_t _appendBuffer(const char *pacSrc, const int iSrcSize,
                             char *pacDst, int *piDstWriteIdx,
                             const int iDstBuffSize) {
  // If nothing to process
  if (iSrcSize == 0) {
    return ESP_OK;
  }
  ESP_LOGV(UI_TAG, "_appendBuffer: srcSize: %i ; Widx: %d ; DstBsize: %i",
           iSrcSize, *piDstWriteIdx, iDstBuffSize);

  for (int iProcByteCnt = 0; iProcByteCnt < iSrcSize; iProcByteCnt++) {
    /* Check if there is "backspace" character. If yes, do not move forward
     * but decrease write pointer, so previous character will be deleted
     */
    if (pacSrc[iProcByteCnt] == '\b') {
      // It is pointless to write "backspace" character - skip it
      (*piDstWriteIdx)--;
      // Check index value
      if (*piDstWriteIdx < 0) {
        *piDstWriteIdx = 0;
      } else {
        /* Print to console space (overwrite old character) and print
         * backspace
         */
        _uiPrintStr(" \b");
      }
    }  ///@todo Handle other non-character symbols
    else {
      // Copy character, increase write index
      pacDst[*piDstWriteIdx] = pacSrc[iProcByteCnt];

      // Increase write index & check value
      (*piDstWriteIdx)++;
      if (*piDstWriteIdx >= iDstBuffSize) {
        ESP_LOGE(UI_TAG, "Not enough memory in input buffer");
        return UI_NO_MEM_ERR;
      }
    }
  }
  return UI_OK;
}

inline static uiErr_t _getMacFromNVS(
    uint8_t au8mac[NVS_MAX_REGISTERED_MAC][MAC_SIZE_BYTES]) {
  teNvsSttngsErr eErrCode = nvsSttngsGetBtMacList(au8mac);

  if (!eErrCode) {
    return UI_OK;
  }
  return UI_NVM_ERR;
}

static void _checkNvsErrCode(teNvsSttngsErr eNvsErr) {
  /* If there is some write error, it is unfortunate, but better
   * to tell user that settings was not saved
   */
  if (eNvsErr) {
    _uiPrintStr(
        "Writing MAC to memory failed!\n"
        "This should not happen,"
        " maybe memory is failing :(\n");
  } else {
    _uiPrintStr(" [OK]");
    // Write was successful -> reboot MCU -> re-load values
    esp_restart();
  }
}

static int _findNewLine(const char *pacBuff, const int iWriteIdx) {
  for (int iIdx = 0; iIdx < iWriteIdx; iIdx++) {
    if ((pacBuff[iIdx] == '\n') || (pacBuff[iIdx] == '\r')) {
      return iIdx;
    }
  }
  return -1;
}

static uiErr_t _StrHex2ByteToInt(const char *pacStr, uint8_t *piResult) {
  // Take two characters (low and high 4 bits) and convert them
  uiErr_t eErr;
  uint8_t iHigh4bits, iLow4bits;

  eErr = _charHexToInt(pacStr[0], &iHigh4bits);
  if (eErr) {
    return eErr;
  }

  eErr = _charHexToInt(pacStr[1], &iLow4bits);
  if (eErr) {
    return eErr;
  }

  // If program get there, conversions were successful -> calculate result
  *piResult = 16 * iHigh4bits + iLow4bits;

  return UI_OK;
}

static uiErr_t _charHexToInt(const char cHex, uint8_t *piResult) {
  // 0~9
  if ((cHex >= '0') && (cHex <= '9')) {
    // Char is just integer -> decrease ACSII "0" and get integer result
    *piResult = cHex - '0';
    return UI_OK;
  } else if ((cHex >= 'A') && (cHex <= 'F')) {
    // Same as before - "Minus A" is the key. Since A means 10 -> +10
    *piResult = cHex - 'A' + 10;
    return UI_OK;
  } else if ((cHex >= 'a') && (cHex <= 'f')) {
    *piResult = cHex - 'a' + 10;
    return UI_OK;
  }

  return UI_CONVERSION_ERR;
}

/**
 * @brief Prints help for user
 * @return Error code - zero if no error, non-zero otherwise.
 */
static uiErr_t _printHelp(void) {
  uiErr_t eErr = _uiPrintStr(pacHelp);
  UI_CHECK_PRINT_ERR_CODE(eErr);
  return UI_OK;
}

/**
 * @brief Simply print MAC
 * @param au8mac Pointer to array where MAC is stored
 * @return ESP_OK if no problem is detected
 */
static uiErr_t _printMac(uint8_t au8mac[MAC_SIZE_BYTES]) {
  uiErr_t eErr;
  char acMacByte[] = "00";

  for (int iCnt = 0; iCnt < 6; iCnt++) {
    sprintf(acMacByte, "%02X", au8mac[iCnt]);
    eErr = _uiPrintStr(acMacByte);
    if (iCnt < 5) {
      eErr |= _uiPrintStr(":");
    }
    UI_CHECK_PRINT_ERR_CODE(eErr);
  }
  return UI_OK;
}

/**
 * @brief Inform user that input MAC is not valid
 * @return UI_OK if no error
 */
static uiErr_t _printInvalidMacInput(void) {
  uiErr_t eErr;

  ESP_LOGD(UI_TAG, "Invalid user input - BT MAC");

  eErr = _uiPrintStr(
      "Can not parse MAC.\n"
      " Please use following format \n11:22:33:44:55:66");

  return eErr;
}

/**
 * @brief Inform user that input scan duration is not valid
 * @return UI_OK if no error
 */
static uiErr_t _printInvalidScanDurationInput(void) {
  uiErr_t eErr;
  ESP_LOGD(UI_TAG, "Invalid user input - Scan duration");

  eErr = _uiPrintStr(
      "Invalid scan duration.\n"
      "Please use value from 1 to 255 range");
  UI_CHECK_PRINT_ERR_CODE(eErr);
  return eErr;
}

/**
 * @brief Inform user that input scan period is not valid
 * @return UI_OK if no error
 */
static uiErr_t _printInvalidScanPeriodInput(void) {
  uiErr_t eErr;
  ESP_LOGD(UI_TAG, "Invalid user input - Scan period");

  eErr = _uiPrintStr(
      "Invalid scan period.\n"
      "Please use value from 1 to 65535 range");
  UI_CHECK_PRINT_ERR_CODE(eErr);
  return eErr;
}

static uiErr_t _printInvalidMinTxPwrInput(void) {
  uiErr_t eErr;
  ESP_LOGD(UI_TAG, "Invalid user input - Min TX power");

  eErr = _uiPrintStr(
      "Invalid minimum TX power.\n"
      "Please use value from " UI_RF_IC_MIN_TX_DBM_STR
      " to " UI_RF_IC_MAX_TX_DBM_STR " range.");
  UI_CHECK_PRINT_ERR_CODE(eErr);
  return eErr;
}

static uiErr_t _printMinTxPwrHigerThanMaxTx(void) {
  uiErr_t eErr;
  ESP_LOGD(UI_TAG, "Min TX power is higher than max TX power");
  eErr = _uiPrintStr("Minimum TX power is higher than maximum TX power!");

  UI_CHECK_PRINT_ERR_CODE(eErr);
  return eErr;
}

static uiErr_t _printInvalidMaxTxPwrInput(void) {
  uiErr_t eErr;
  ESP_LOGD(UI_TAG, "Invalid user input - Max TX power");

  eErr = _uiPrintStr(
      "Invalid maximum TX power.\n"
      "Please use value from " UI_RF_IC_MIN_TX_DBM_STR
      " to " UI_RF_IC_MAX_TX_DBM_STR " range.");
  UI_CHECK_PRINT_ERR_CODE(eErr);
  return eErr;
}

static uiErr_t _printMaxTxPwrLowerThanMinTx(void) {
  uiErr_t eErr;
  ESP_LOGD(UI_TAG, "Max TX power is lower than min TX power");
  eErr = _uiPrintStr("Maximum TX power is lower than minimum TX power!");

  UI_CHECK_PRINT_ERR_CODE(eErr);
  return eErr;
}

static uiErr_t _printInvalidDropOffCntInput(void) {
  uiErr_t eErr;
  ESP_LOGD(UI_TAG, "Invalid user input - Drop off count");
  eErr = _uiPrintStr(
      "Invalid drop off counter value\n"
      "Please use value from range 0~255");

  UI_CHECK_PRINT_ERR_CODE(eErr);
  return eErr;
}

static uiErr_t _printInvalidExtLoadInput(void) {
  uiErr_t eErr;
  ESP_LOGD(UI_TAG, "Invalid user input - external load level");
  eErr = _uiPrintStr(
      "Invalid external load value\n"
      "Please use value from range 0~4");

  UI_CHECK_PRINT_ERR_CODE(eErr);
  return eErr;
}

static uiErr_t _printNoFreeSlotForNewMac(void) {
  uiErr_t eErr;
  ESP_LOGD(UI_TAG, "No free slots for new MAC");
  eErr = _uiPrintStr(
      "No free slots for registering new MAC\n"
      "You need to purge MAC list");

  UI_CHECK_PRINT_ERR_CODE(eErr);
  return eErr;
}

static void _disableAllExtraLoad(void) {
  gpio_pad_select_gpio(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_0);
  gpio_set_direction(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_0, GPIO_MODE_DISABLE);

  gpio_pad_select_gpio(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_1);
  gpio_set_direction(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_1, GPIO_MODE_DISABLE);

  gpio_pad_select_gpio(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_2);
  gpio_set_direction(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_2, GPIO_MODE_DISABLE);

  gpio_pad_select_gpio(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_3);
  gpio_set_direction(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_3, GPIO_MODE_DISABLE);
}

static void _enableExtraLoad(gpio_num_t iGpioNumber) {
  gpio_pad_select_gpio(iGpioNumber);
  gpio_set_direction(iGpioNumber, GPIO_MODE_OUTPUT);
  gpio_set_level(iGpioNumber, 1);
}

static void _setExtraLoadLevel(uint8_t u8loadLevel) {
  // Default is turning off all GPIO
  _disableAllExtraLoad();

  ESP_LOGD(UI_TAG, "Setting load level to %u", u8loadLevel);

  switch (u8loadLevel) {
    case 0:
      // Keep as it is.
      break;
    case 1:
      // Just PIN 0
      _enableExtraLoad(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_0);
      break;
    case 2:
      // PIN 0 & 1
      _enableExtraLoad(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_0);
      _enableExtraLoad(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_1);
      break;
    case 3:
      // PIN 0, 1, 2
      _enableExtraLoad(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_0);
      _enableExtraLoad(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_1);
      _enableExtraLoad(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_2);
      break;
    case 4:
      // PIN 0, 1, 2, 3
      _enableExtraLoad(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_0);
      _enableExtraLoad(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_1);
      _enableExtraLoad(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_2);
      _enableExtraLoad(UI_DEFAULT_EXTRA_PWR_LOAD_PIN_3);
      break;
    default:
      // This should not happen, but it is not critical too
      ESP_LOGE(UI_TAG, "Unsupported load level: %u", u8loadLevel);
  }
}
