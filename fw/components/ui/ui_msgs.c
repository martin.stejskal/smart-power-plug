/**
 * @file
 *
 * @brief Kind of language file
 *
 * Contains all main messages. It does NOT contain error messages
 *
 * @author Martin Stejskal
 *
 */

/**
 * @brief TX output power for RF IC as string
 *
 * This should not be changed. Only helping generate help. For range please
 * refer to esp_power_level_t enum.
 *
 * @{
 */
#define UI_RF_IC_MIN_TX_DBM_STR "-12"
#define UI_RF_IC_MAX_TX_DBM_STR "9"
/**
 * @}
 */

/**
 * @brief Set of commands which are used
 *
 * @{
 */
#define UI_BT_MAC_LIST_GET_CMD "bt mac list"
#define UI_BT_MAC_SET_CMD "bt mac "
#define UI_BT_MAC_LIST_DEL_CMD "bt del mac"
#define UI_SCAN_DUR_GET_CMD "scan duration"
#define UI_SCAN_DUR_SET_CMD "scan duration "
#define UI_SCAN_PERIOD_GET_CMD "scan period"
#define UI_SCAN_PERIOD_SET_CMD "scan period "
#define UI_MIN_TX_PWR_GET_CMD "tx pwr min"
#define UI_MIN_TX_PWR_SET_CMD "tx pwr min "
#define UI_MAX_TX_PWR_GET_CMD "tx pwr max"
#define UI_MAX_TX_PWR_SET_CMD "tx pwr max "
#define UI_DROP_OFF_GET_CMD "drop off"
#define UI_DROP_OFF_SET_CMD "drop off "
#define UI_EXT_LOAD_GET_CMD "ext load"
#define UI_EXT_LOAD_SET_CMD "ext load "
#define UI_DEFAULT_CMD "default"
/**
 * @}
 */
static const char *pacHelp =
    "\n\n==<| Help |>==\n"
    " Here is list of available commands:\n"
    "   " UI_BT_MAC_LIST_GET_CMD
    " - Return list of BT MAC on which is power plug triggered\n"

    "   " UI_BT_MAC_SET_CMD
    "11:22:33:44:55:66 - Set BT MAC trigger\n"

    "   " UI_BT_MAC_LIST_DEL_CMD
    " - Delete all registered MAC\n"

    "   " UI_SCAN_DUR_GET_CMD
    " - Return actual scan duration value in seconds\n"

    "   " UI_SCAN_DUR_SET_CMD
    "10 - Set scan duration to 10 seconds for example\n"

    "   " UI_SCAN_PERIOD_GET_CMD
    " - Return actual scanning period in seconds\n"

    "   " UI_SCAN_PERIOD_SET_CMD
    "60 - Set scan period to 60 seconds for instance\n"

    "   " UI_MIN_TX_PWR_GET_CMD
    " - Get minimum TX power for Bluetooth in dBm\n"

    "   " UI_MIN_TX_PWR_SET_CMD
    "-9 - Set min TX power to -9 dBm. Step is 3. "
    "Range: " UI_RF_IC_MIN_TX_DBM_STR " ~ " UI_RF_IC_MAX_TX_DBM_STR
    "\n"

    "   " UI_MAX_TX_PWR_GET_CMD
    " - Get maximum TX power for Bluetooth in dBm\n"

    "   " UI_MAX_TX_PWR_SET_CMD
    "6 - Set max TX power to +6 dBm. Step is 3. Range: " UI_RF_IC_MIN_TX_DBM_STR
    " ~ " UI_RF_IC_MAX_TX_DBM_STR
    "\n"

    "   " UI_DROP_OFF_GET_CMD
    " - Return actual drop off count\n"

    "   " UI_DROP_OFF_SET_CMD
    "5 - Set drop off count to 3 for example\n"

    "   " UI_EXT_LOAD_GET_CMD
    " - Get actual external load level. 0~4\n"
    "              (no power waste ~ max power waste)\n"

    "   " UI_EXT_LOAD_SET_CMD
    "2 - Set external load level to 2 (~ +20 mA power consumption)\n"

    "   " UI_DEFAULT_CMD " - restore default values\n";

static const char *psCanNotPrintToUart = "Can not print to user UART!";
///@todo Move more messages here
