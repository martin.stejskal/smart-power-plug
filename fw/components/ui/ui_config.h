/**
 * @file
 * @author Martin Stejskal
 * @brief Setting file for UI
 */
#ifndef __UI_CONFIG_H__
#define __UI_CONFIG_H__
//================================| Includes |================================
#include <driver/gpio.h>
#include <driver/uart.h>
// When compiler support it, check if user file exists and if yes, include it
#if defined __has_include
#if __has_include("ui_config_user.h")
#include "ui_config_user.h"
#endif
#endif
// ================================| Defines |================================
// ============================| Default values |=============================
#ifndef UI_UART_BAUDRATE
#define UI_UART_BAUDRATE (115200)
#endif

#ifndef UI_UART_INTERFACE
#define UI_UART_INTERFACE (UART_NUM_1)
#endif

#ifndef UI_TXD_PIN
#define UI_TXD_PIN (GPIO_NUM_17)
#endif

#ifndef UI_RXD_PIN
#define UI_RXD_PIN (GPIO_NUM_16)
#endif

#ifndef UI_RX_BUFFER_SIZE
/**
 * @brief Buffer size in Bytes
 *
 * Usually commands should be really short, so something like 256 should be
 * more than enough.
 *
 * @note For some reason, size under 128 is not allowed. Just be warned
 */
#define UI_RX_BUFFER_SIZE (512)
#endif

#ifndef UI_RX_USER_INPUT_TIMEOUT_MS
/**
 * @brief Define period in ms within UART RX function stop scanning
 *
 * For best user experience (fast reply from MCU) keep this value set to 1
 */
#define UI_RX_USER_INPUT_TIMEOUT_MS (1)
#endif

#ifndef UI_TASK_PERIOD_MS
/**
 * @brief Some reasonable period within UI task is called in ms
 *
 * Recommended value is 50
 */
#define UI_TASK_PERIOD_MS (35)
#endif

#ifndef UI_DEFAULT_SCAN_DURATION_S
/**
 * @brief Single scan duration in seconds
 */
#define UI_DEFAULT_SCAN_DURATION_S (10)
#endif  // UI_DEFAULT_SCAN_DURATION_S

#ifndef UI_DEFAULT_SCAN_PERIOD_S
/**
 * @brief Scan period in seconds
 *
 * This value does not take into account scan duration, so set it wisely.
 */
#define UI_DEFAULT_SCAN_PERIOD_S (30)
#endif  // UI_DEFAULT_SCAN_PERIOD_S

#ifndef UI_DEFAULT_MIN_TX_PWR_DBM
/**
 * @brief Default minimum TX power in dBm for RF part
 */
#define UI_DEFAULT_MIN_TX_PWR_DBM (-12)
#endif  // UI_DEFAULT_MIN_TX_PWR_DBM

#ifndef UI_DEFAULT_MAX_TX_PWR_DBM
/**
 * @brief Default maximum TX power in dBm for RF part
 */
#define UI_DEFAULT_MAX_TX_PWR_DBM (9)
#endif  // UI_DEFAULT_MAX_TX_PWR_DBM

#ifndef UI_DEFAULT_DROP_OFF_CNT
/**
 * @brief When device is not detected, try to scan X times before shutdown plug
 *
 * When reference device is not found, it might be caused by interference or
 * some bug at ESP BT module. Simply program should not turn off power plug
 * immediately, but perform few more scans to be sure. This value defines how
 * many extra scans should be done, before it is decided to finally shutdown
 * power plug
 */
#define UI_DEFAULT_DROP_OFF_CNT (3)
#endif  // UI_DEFAULT_DROP_OFF_CNT

#ifndef UI_DEFAULT_EXTRA_PWR_LOAD_LVL
/**
 * @brief Kind of reverse to power saving level
 *
 * Range 0~4. Higher value enable additional loads and therefore increasing
 * total power consumption. Reason is that some AC/DC converters can generate
 * high pitch noise, which can be really annoying if you can hear it.
 * Default should be 0 and value should be modified on custom device via
 * user interface. Same way as is set Bluetooth MAC
 */
#define UI_DEFAULT_EXTRA_PWR_LOAD_LVL (0)
#endif  // UI_DEFAULT_EXTRA_PWR_LOAD_LVL

/**
 * @brief Pin mapping to the external load resistors
 *
 * Values as pin numbers. For example value 5 means pin IO5.
 * It is expected that these pins are connected 330 Ohm resistors to the ground
 * to drain more current from power supply. Sounds crazy, but most of AC/DC
 * converters generating high pitch sound, which might be annoying. Additional
 * load will solve it. Due to relatively low current consumption it is "OK".
 * Extra power consumption is about 30 mW per resistor.
 *
 * @{
 */
#ifndef UI_DEFAULT_EXTRA_PWR_LOAD_PIN_0
///@brief TP10
#define UI_DEFAULT_EXTRA_PWR_LOAD_PIN_0 (12)
#endif  // UI_DEFAULT_EXTRA_PWR_LOAD_PIN_0

#ifndef UI_DEFAULT_EXTRA_PWR_LOAD_PIN_1
///@brief TP11
#define UI_DEFAULT_EXTRA_PWR_LOAD_PIN_1 (13)
#endif  // UI_DEFAULT_EXTRA_PWR_LOAD_PIN_1

#ifndef UI_DEFAULT_EXTRA_PWR_LOAD_PIN_2
///@brief TP12
#define UI_DEFAULT_EXTRA_PWR_LOAD_PIN_2 (14)
#endif  // UI_DEFAULT_EXTRA_PWR_LOAD_PIN_2

#ifndef UI_DEFAULT_EXTRA_PWR_LOAD_PIN_3
///@brief TP13
#define UI_DEFAULT_EXTRA_PWR_LOAD_PIN_3 (22)
#endif  // UI_DEFAULT_EXTRA_PWR_LOAD_PIN_3
/**
 * @}
 */
//===========================| Preprocessor checks |==========================
//============================| Structures, enums |===========================
#endif  // __UI_CONFIG_H__
