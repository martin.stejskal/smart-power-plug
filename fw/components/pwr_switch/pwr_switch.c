/**
 * @file
 * @author Martin Stejskal
 * @brief Component that handle power switch
 */
// ===============================| Includes |================================
#include "pwr_switch.h"

#include "pwr_switch_config.h"

// ESP specific libraries
#include <driver/gpio.h>
#include <esp_log.h>
// ================================| Defines |================================
/**
 * @brief Tag for logger
 */
static const char *PWR_SW_TAG = "PWR switch";
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Keep track about current mode
 */
static pwrSwMode_t geMode = PWR_SW_MODE_BT;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief Enable or disable power switch
 * @param u8pwrEn If non-zero, enable power switch. Otherwise it will disable
 *                it
 */
static void _setGpio(uint8_t u8pwrEn);
// =========================| High level functions |==========================

void pwrSwInit(void) {
  // Setup GPIO - off by default (fail-safe)
  gpio_pad_select_gpio(GPIO_POWER_SWITCH);
  gpio_set_direction(GPIO_POWER_SWITCH, GPIO_MODE_OUTPUT);
  gpio_set_level(GPIO_POWER_SWITCH, 0);

  // Set default mode
  geMode = PWR_SW_MODE_BT;

  ESP_LOGD(PWR_SW_TAG, "Initialization done");
}

pwrSwErr_t pwrSwSetMode(pwrSwMode_t eMode) {
  switch (eMode) {
    case PWR_SW_MODE_BT:
      geMode = eMode;
      ESP_LOGD(PWR_SW_TAG, "Setting Bluetooth mode");
      break;
    case PWR_SW_MODE_MANUAL:
      geMode = eMode;
      ESP_LOGD(PWR_SW_TAG, "Setting Manual mode");
      break;
    default:
      ESP_LOGE(PWR_SW_TAG, "Unexpected mode: %d", eMode);
      return PWR_SW_INVALID_PARAM;
  }
  return PWR_SW_OK;
}

inline pwrSwMode_t pwrSwGetMode(void) { return geMode; }

void pwrSwBtSet(uint8_t u8pwrEn) {
  // If actual mode is not set to "Bluetooth", ignore request
  if (geMode != PWR_SW_MODE_BT) {
    ESP_LOGD(PWR_SW_TAG, "Bluetooth request - ignored (different mode)");
    return;
  }

  ESP_LOGI(PWR_SW_TAG, "Bluetooth request - set power switch: %d", u8pwrEn);
  _setGpio(u8pwrEn);
}

void pwrSwManualSet(uint8_t u8pwrEn) {
  // If actual mode is not set to "manual", ignore request
  if (geMode != PWR_SW_MODE_MANUAL) {
    ESP_LOGD(PWR_SW_TAG, "Manual request - ignored (different mode)");
    return;
  }

  ESP_LOGI(PWR_SW_TAG, "Manual request - set power switch: %d", u8pwrEn);
  _setGpio(u8pwrEn);
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
static void _setGpio(uint8_t u8pwrEn) {
  if (u8pwrEn) {
    gpio_set_level(GPIO_POWER_SWITCH, 1);
  } else {
    gpio_set_level(GPIO_POWER_SWITCH, 0);
  }
}
