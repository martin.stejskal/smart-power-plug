/**
 * @file
 * @author Martin Stejskal
 * @brief Component that handle power switch
 */
#ifndef __PWR_SWITCH_H__
#define __PWR_SWITCH_H__
// ===============================| Includes |================================
#include <stdint.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum { PWR_SW_MODE_BT, PWR_SW_MODE_MANUAL } pwrSwMode_t;

typedef enum {
  PWR_SW_OK,
  PWR_SW_ERROR,
  PWR_SW_INVALID_PARAM,
} pwrSwErr_t;
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Initialize GPIO which control power switch
 */
void pwrSwInit(void);

/**
 * @brief Set current mode
 *
 * Request to on/off power switch can come from different places, but for
 * example request from button (manual switch on/off) should superseded request
 * from Bluetooth.
 * @param Mode type
 * @return PWR_SW_OK if no error
 */
pwrSwErr_t pwrSwSetMode(pwrSwMode_t eMode);

/**
 * @brief Return actual mode
 * @return Actual mode
 */
pwrSwMode_t pwrSwGetMode(void);

/**
 * @brief Request from Bluetooth to on/off power switch
 * @param u8pwrEn Set 0 to disable power switch, non-zero otherwise
 */
void pwrSwBtSet(uint8_t u8pwrEn);

/**
 * @brief Request from manual button press to on/off power switch
 * @param u8pwrEn Set 0 to disable power switch, non-zero otherwise
 */
void pwrSwManualSet(uint8_t u8pwrEn);

// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
#endif  // __PWR_SWITCH_H__
