/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for power switch component
 */
#ifndef __PWR_SWITCH_CONFIG_H__
#define __PWR_SWITCH_CONFIG_H__
// ===============================| Includes |================================
// When compiler support it, check if user file exists and if yes, include it
#if defined __has_include
#if __has_include("pwr_switch_config_user.h")
#include "pwr_switch_config_user.h"
#endif
#endif
// ================================| Defines |================================
// ============================| Default values |=============================
#ifndef GPIO_POWER_SWITCH
/**
 * @brief Pin which control power plug
 */
#define GPIO_POWER_SWITCH (GPIO_NUM_2)
#endif  // GPIO_POWER_SWITCH
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
#endif  // __PWR_SWITCH_CONFIG_H__
