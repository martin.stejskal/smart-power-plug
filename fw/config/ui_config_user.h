// Most of settings is re-defined at following file
#include "smart_power_plug_config.h"
#define UI_DEFAULT_MAC              BT_DEFAULT_MAC
#define UI_DEFAULT_SCAN_DURATION_S  BT_SCAN_DURATION_S
#define UI_DEFAULT_SCAN_PERIOD_S    BT_SCAN_PERIOD_S
#define UI_DEFAULT_MIN_TX_PWR_DBM   BT_MIN_TX_PWR_DBM
#define UI_DEFAULT_MAX_TX_PWR_DBM   BT_MAX_TX_PWR_DBM
#define UI_DEFAULT_DROP_OFF_CNT     BT_DROP_OFF_CNT
