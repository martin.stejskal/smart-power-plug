/**
 * @file
 *
 * @author Martin Stejskal
 *
 * @brief Configuration file
 */
#ifndef __SMRT_PWR_PLG_CFG_H__
#define __SMRT_PWR_PLG_CFG_H__
// ============================| Default values |=============================
/* For detailed info search in "components" folder in "*_config.h" files.
 * Here are these "default values" only re-written so everything is at one
 * place instead of distributed everywhere while single components have all
 * necessary doc and "default values" too.
 *
 * Here are most common "variables" while in these configurations can be
 * some other fine settings, which 99% people do not care about.
 */
#define BT_SCAN_DURATION_S (5)
#define BT_SCAN_PERIOD_S (30)

#define BT_MIN_TX_PWR_DBM (-12)
#define BT_MAX_TX_PWR_DBM (-9)

#define BT_DROP_OFF_CNT (3)

#define GPIO_POWER_SWITCH (GPIO_NUM_2)

#define UI_UART_BAUDRATE (115200)
#define UI_UART_INTERFACE (UART_NUM_2)
#define UI_TXD_PIN (GPIO_NUM_17)
#define UI_RXD_PIN (GPIO_NUM_16)

#define BTN_MANUAL_SWITCH (GPIO_NUM_5)
#define BTN_LED_BT_ON (GPIO_NUM_18)
#define BTN_LED_MANUAL_ON (GPIO_NUM_19)
#define BTN_LED_BTN_PRESSED (GPIO_NUM_21)
#define BTN_LED_ON_PERIOD_MS (5000)

#define SHOW_STACK_USAGE (0)
#endif  // __SMRT_PWR_PLG_CFG_H__
