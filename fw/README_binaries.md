# About
 * Deployed binaries for
   [Smart power plug](https://gitlab.com/martin.stejskal/smart-power-plug)

# How to flash
 * In general, you can follow any ESP32 tutorial. However here is brief manual:
   * Connect ESP32 to PC. Typically ESP32 development kits have USB<-->UART
     converter on board.
   * If you do not have [esptool](https://github.com/espressif/esptool)
     installed yet, install it. If you like Python virtual environment, you
     can run `source_me.sh` script on Linux.

```bash
$ source source_me.sh
```

   * Assume that ESP32 UART is `/dev/ttyUSB0`, run following command

```bash
$ esptool.py -p /dev/ttyUSB0 write_flash \
  0x1000 bootloader.bin 0x8000 partition-table.bin 0x10000 smart_power_plug.bin
```

   * You might need to press "BOOT" button on some development kits.
