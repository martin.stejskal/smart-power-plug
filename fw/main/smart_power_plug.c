/*
 This example code is in the Public Domain (or CC0 licensed, at your option.)

 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.
 */

/****************************************************************************
 *
 * This file is for Classic Bluetooth device and service discovery Demo.
 *
 ****************************************************************************/

#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <nvs_flash.h>
#include <stdint.h>
#include <string.h>

#include "bt_scan.h"
#include "button.h"
#include "pwr_switch.h"
#include "smart_power_plug_config.h"
#include "ui.h"

// ==========================| Preprocessor checks |==========================
// ================================| Defines |================================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
#if SHOW_STACK_USAGE
static const char *STACK_TAG = "Stack usage";
/**
 * @brief Task which periodically print stack usage
 * @param pvParameters Parameters - not used actually
 */
void showStackUsage(void *pvParameters) {
  UBaseType_t uNumOfTasks;
  TaskStatus_t *pxTaskStatusArray;
  uint32_t u32TotalRunTime;

  for (;;) {
    vTaskDelay(5000 / portTICK_PERIOD_MS);
    ESP_LOGI(STACK_TAG, "=================================");

    // Take a snapshot of the number of tasks in case it changes while this
    // function is executing.
    uNumOfTasks = uxTaskGetNumberOfTasks();
    // Allocate a TaskStatus_t structure for each task.  An array could be
    // allocated statically at compile time.
    pxTaskStatusArray = pvPortMalloc(uNumOfTasks * sizeof(TaskStatus_t));

    // Pointer should not be empty, but just in case check it
    if (pxTaskStatusArray) {
      uxTaskGetSystemState(pxTaskStatusArray, uNumOfTasks, &u32TotalRunTime);
      for (int iTskCnt = 0; iTskCnt < uNumOfTasks; iTskCnt++) {
        ESP_LOGI(STACK_TAG, "%s | Free Bytes: %d",
                 pxTaskStatusArray[iTskCnt].pcTaskName,
                 pxTaskStatusArray[iTskCnt].usStackHighWaterMark);
      }
    }

    // The array is no longer needed, free the memory it consumes.
    vPortFree(pxTaskStatusArray);
    ESP_LOGI(STACK_TAG, "=================================");
  }
}
#endif

void app_main() {
  // We need to run program from external flash -> initialize it
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES ||
      ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);

  // Set power switch
  pwrSwInit();

  // Create tasks - give it quite low priority (1)
  xTaskCreate(uiTaskRTOS, "ui task", 4 * 1024, NULL, 1, NULL);
  xTaskCreate(buttonTaskRTOS, "btn task", 2 * 1024, NULL, 2, NULL);
  xTaskCreate(btScanTaskRTOS, "scan task", 3 * 1024, NULL, 3, NULL);

#if SHOW_STACK_USAGE
  xTaskCreate(showStackUsage, "task usage", 2 * 1024, NULL, 4, NULL);
#endif
}
