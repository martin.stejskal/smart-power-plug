#/bin/bash

# Put dump without == CORE DUMP START/END == into /tmp/dump
"$IDF_PATH"/components/espcoredump/espcoredump.py info_corefile -t b64 -c /tmp/dump build/smart_power_plug.elf
